mainApp.controller('candidate', ['$scope', 'httpHandler', '$filter', '$attrs', function ($scope, httpHandler, $filter, $attrs) {

	$scope.no = 1;
	$scope.itemsPerPage = 10;
	$scope.keyword = {};


	$scope.getCandidate = function (pageno) {
		$scope.pending = true;
		$scope.totalData = 0;
		$scope.itemData = {};

		if (pageno == 1)
			$scope.no = 1;
		else
			$scope.no = (pageno * $scope.itemsPerPage) - ($scope.itemsPerPage - 1);


		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'candidate/get_candidate',
			params: {
				page: pageno,
				limit: $scope.itemsPerPage,
				keyword: $scope.keyword,
				column: true,
			}
		}).then(
			function successCallbacks(response) {
				$scope.pending = false;
				$scope.loader_listcategory = false;
				$scope.currentPage = response.data.page;
				$scope.itemData = response.data.data;

				$scope.totalData = response.data.total;
				$scope.last_update_data = response.data.last_update;

			},
			function errorCallback(response) {

			}
		);
	}

	if ($attrs.id == 'candidate') {
		$scope.getCandidate(1);
	}

	$scope.invite = function (value) {
		$("#mdlInvite").modal("show");
		$scope.candidate = {
			email: value.email
		};
	}
}]);
