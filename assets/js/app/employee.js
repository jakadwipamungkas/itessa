mainApp.controller('employee', ['$scope', 'httpHandler', '$filter', '$attrs', function ($scope, httpHandler, $filter, $attrs) {
	$scope.id_employee = $attrs.idEmployee;

	const Toast = Swal.mixin({
		toast: true,
		position: "top-end",
		showConfirmButton: false,
		timer: 3500,
		timerProgressBar: false,
		allowEscapeKey: false,
		allowOutsideClick: false,
		showClass: {
			popup: "animated lightSpeedIn",
		},
		hideClass: {
			popup: "animated lightSpeedOut",
		},
		onOpen: (toast) => {
			toast.addEventListener("mouseenter", Swal.stopTimer);
			toast.addEventListener("mouseleave", Swal.resumeTimer);
		},
	});

	$scope.tinymceOptions = {
		relative_urls: false,
		remove_script_host: false,
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons paste textcolor filemanager code"
		],
		toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | code image media link',
		height: 300,
		filemanager_title: 'File Manager',
		filemanager_access_key: "CHrfEWLWhBVgAkKqu6OY",
		external_filemanager_path: "../bower_components/tinymce/plugins/filemanager/",
		external_plugins: {
			"filemanager": "./plugins/filemanager/plugin.min.js"
		}
	}

	var real_url = window.location.pathname;
	var url_arr = real_url.split("/");

	if (url_arr[3] == 'detail') {
		$scope.url_edited = urls + url_arr[2] + '/' + 'getUpdate' + '?id_employee=' + $scope.id_employee;
	}

	// assets selector
	$scope.getCountry = function () {
		httpHandler.send({
			method: 'GET',
			url: urls + 'employee/getCountry',
		}).then(
			function successCallbacks(response) {
				$scope.dataCountry = response.data;
			},
			function errorCallback(response) {
				console.log(response);
			}
		);
	}

	$scope.getSocialMedia = function () {
		httpHandler.send({
			method: 'GET',
			url: urls + 'employee/getSocialMedia',
		}).then(
			function successCallbacks(response) {
				$scope.dataSocialMedia = response.data;
			},
			function errorCallback(response) {
				console.log(response);
			}
		);
	}
	// end assets selector

	$scope.setTemporary = function () {
		Swal.fire({
			title: 'Loading...',
			allowEscapeKey: false,
			allowOutsideClick: false,
			showConfirmButton: false,
			imageUrl: urls + "assets/img/loadertsel.gif",
		});

		var params = {
			id: $scope.id_employee,
		};

		httpHandler.send({
			method: 'GET',
			url: urls + 'employee/saveTemporary',
			params
		}).then(
			function successCallbacks(response) {
				$scope.getDetailEmployee();
			},
			function errorCallback(response) {
				console.log(response);
			}
		);
	}

	$scope.getDetailEmployee = function () {
		var params = {
			id: $scope.id_employee,
			menu: url_arr[3]
		};

		httpHandler.send({
			method: 'GET',
			url: urls + 'employee/getDetail',
			params
		}).then(
			function successCallbacks(response) {
				Swal.close();
				$scope.personal = response.data.personal;

				if ($scope.personal.status_user == 1) {
					$scope.status_user = 'Employe Status: Draft';
				} else if ($scope.personal.status_user == 2) {
					$scope.status_user = 'Employe Status: Submited';
				} else if ($scope.personal.status_user == 3) {
					$scope.status_user = 'Employe Status: Rejected HCM';
				} else if ($scope.personal.status_user == 4) {
					$scope.status_user = 'Employe Status: Done';
				} else {
					$scope.status_user = 'Employe Status: Incomplete';
				}

				$scope.media_social = response.data.social_media_accounts;
				$scope.education = response.data.education;
				$scope.family = response.data.family;
				$scope.language = response.data.language;
				$scope.membership = response.data.membership;
				$scope.experiences = response.data.experiences;
				$scope.medical = response.data.medical;
				$scope.references = response.data.references;
				$scope.award = response.data.award;
				if (url_arr[3] == 'getUpdate') {
					$scope.showPersonal();
				}
			},
			function errorCallback(response) {
				console.log(response);
			}
		);
	}

	if (url_arr[3] == 'detail') {
		$scope.setTemporary();
	}

	if (url_arr[3] == 'getUpdate') {
		$scope.getDetailEmployee();
	}

	$scope.showPersonal = function () {
		$scope.card_personal = true;
		$scope.getCountry();
		$scope.getSocialMedia();
		$scope.nik = $scope.personal.nik;
		$scope.email_ldap = $scope.personal.email_ldap;
		$scope.full_name = $scope.personal.full_name;
		$scope.nick_name = $scope.personal.nick_name;
		$scope.date_of_birth = $scope.personal.date_of_birth;
		$scope.date_of_place = $scope.personal.date_of_place;
		$scope.photo_profile = $scope.personal.photo_profile;
		$scope.photo_profile_name = $scope.personal.photo_profile_name;
		$scope.citizen_file = $scope.personal.citizen_file;
		$scope.citizen_type_file = $scope.personal.citizen_type_file;
		$scope.citizen_file_name = $scope.personal.citizen_file_name;
		$scope.sex = $scope.personal.sex == 'Male' ? 1 : 2;
		$scope.marital_status = $scope.personal.marital_status == 'Single' ? 1 : 2;
		$scope.nationality = $scope.personal.nationality;
		$scope.citizen_number = parseInt($scope.personal.citizen_number);
		$scope.height = parseInt($scope.personal.height);
		$scope.weight = parseInt($scope.personal.weight);
		$scope.blood_type = $scope.personal.blood_type;
		$scope.religion = $scope.personal.religion;
		$scope.mobile_number = parseInt($scope.personal.mobile_number);
		$scope.email = $scope.personal.email;
		$scope.social_media_app = $scope.personal.social_media_app;
		$scope.social_media_accounts = $scope.personal.social_media_accounts;
		$scope.current_address = $scope.personal.current_address;
		$scope.current_city_address = $scope.personal.current_city_address;
		$scope.formal_address = $scope.personal.formal_address;
		$scope.formal_city_address = $scope.personal.formal_city_address;
	}

	$scope.savePersonal = function () {
		console.log($scope.sex);
	}

}]);

$(document).ready(function () {
	$('.live-search').select2();
});

$('.autoclose-datepicker').datepicker({
	autoclose: true,
	locale: 'id',
	format: 'yyyy-mm-dd'
});
