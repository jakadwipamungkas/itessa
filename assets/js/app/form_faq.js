mainApp.controller('form_faq', ['$scope', 'httpHandler', '$filter', '$attrs', function ($scope, httpHandler, $filter, $attrs) {
	const Toast = Swal.mixin({
		toast: true,
		position: "top-end",
		showConfirmButton: false,
		timer: 3500,
		timerProgressBar: false,
		allowEscapeKey: false,
		allowOutsideClick: false,
		showClass: {
			popup: "animated lightSpeedIn",
		},
		hideClass: {
			popup: "animated lightSpeedOut",
		},
		onOpen: (toast) => {
			toast.addEventListener("mouseenter", Swal.stopTimer);
			toast.addEventListener("mouseleave", Swal.resumeTimer);
		},
	});
	// $scope.id = ;

	$scope.tinymceOptions = {
		relative_urls: false,
		remove_script_host: false,
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons paste textcolor filemanager code"
		],
		toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | code image media link',
		height: 300,
		filemanager_title: 'File Manager',
		filemanager_access_key: "CHrfEWLWhBVgAkKqu6OY",
		external_filemanager_path: "../bower_components/tinymce/plugins/filemanager/",
		external_plugins: {
			"filemanager": "./plugins/filemanager/plugin.min.js"
		}
	}

	$scope.id = !$("#id").val() ? false : $("#id").val();

	if ($scope.id) {
		Swal.fire({
			title: 'Loading...',
			allowEscapeKey: false,
			allowOutsideClick: false,
			showConfirmButton: false,
			imageUrl: urls + "assets/img/loadertsel.gif",
		});
		$scope.getFaqById = function () {
			var params = {
				id: $scope.id
			}

			httpHandler.send({
				method: 'GET',
				url: urls + 'faq/getFaqById',
				params: params
			}).then(
				function successCallbacks(response) {
					Swal.close();
					$scope.title = response.data.data.title;
					$scope.description = response.data.data.description;
				},
				function errorCallback(response) {
					Swal.close();
					Swal.fire({
						title: 'Failed',
						text: response.data.message,
						icon: response.data.status == 500 ? 'error' : 'warning',
						showCancelButton: false,
						allowEscapeKey: false,
						allowOutsideClick: false,
						confirmButtonColor: "#fc544b",
						confirmButtonText: "Okey, otw list!",
					}).then((result) => {
						if (result.value) {
							window.location.replace(urls + 'faq');
						}
					});
				});
		}

		$scope.getFaqById();
	}

	$scope.prosesFaq = function () {
		if (!$scope.title || !$scope.description) {
			return Toast.fire({
				icon: "warning",
				title: 'All fields are required!',
			});
		}
		var formData = new FormData();
		if ($scope.id) {
			formData.append("id", $scope.id);
		}
		formData.append("title", $scope.title);
		formData.append("description", $scope.description);

		Swal.fire({
			title: 'Loading...',
			allowEscapeKey: false,
			allowOutsideClick: false,
			showConfirmButton: false,
			imageUrl: urls + "assets/img/loadertsel.gif",
		});

		httpHandler.send({
			url: urls + 'faq/prosesFaq',
			data: formData,
			method: 'POST',
			headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {
				Swal.close();
				$scope.clearForm();
				Swal.fire({
					title: 'Success',
					text: response.data.message,
					icon: 'success',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#39edab",
					confirmButtonText: "Okey, otw list!",
				}).then((result) => {
					if (result.value) {
						window.location.replace(urls + 'faq');
					}
				});
			},
			function errorCallback(response) {
				Swal.close();
				Swal.fire({
					title: 'Failed',
					text: response.data.message,
					icon: response.data.status == 500 ? 'error' : 'warning',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#fc544b",
					confirmButtonText: "Okey, refresh page",
				}).then((result) => {
					if (result.value) {
						location.reload();
					}
				});
			});
	}

	$scope.clearForm = function () {
		var formData = new FormData();
		$scope.title = null;
		$scope.description = null;
	}
}]);
