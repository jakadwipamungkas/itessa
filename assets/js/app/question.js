mainApp.controller('question', ['$scope', 'httpHandler', '$filter', '$attrs', function ($scope, httpHandler, $filter, $attrs) {
	const Toast = Swal.mixin({
		toast: true,
		position: "top-end",
		showConfirmButton: false,
		timer: 3500,
		timerProgressBar: false,
		allowEscapeKey: false,
		allowOutsideClick: false,
		showClass: {
			popup: "animated lightSpeedIn",
		},
		hideClass: {
			popup: "animated lightSpeedOut",
		},
		onOpen: (toast) => {
			toast.addEventListener("mouseenter", Swal.stopTimer);
			toast.addEventListener("mouseleave", Swal.resumeTimer);
		},
	});

	$scope.tinymceOptions = {
		relative_urls: false,
		remove_script_host: false,
		plugins: [
			"advlist autolink link image lists charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
			"table contextmenu directionality emoticons paste textcolor filemanager code"
		],
		toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | code image media link',
		height: 300,
		filemanager_title: 'File Manager',
		filemanager_access_key: "CHrfEWLWhBVgAkKqu6OY",
		external_filemanager_path: "../bower_components/tinymce/plugins/filemanager/",
		external_plugins: {
			"filemanager": "./plugins/filemanager/plugin.min.js"
		}
	}

	// special assets
	$scope.id_question = !$attrs.idquestion ? null : $attrs.idquestion;
	$scope.url_list = urls + 'question/listQuestion';
	var real_url = window.location.pathname;
	var url_arr = real_url.split("/");
	// end special assets

	$scope.pageno = 1;
	$scope.itemsPerPage = 10;
	$scope.keyword = null;
	$scope.total_count = 0;
	$scope.message = null;

	$scope.getQuestion = function (pageno = 1) {
		$scope.total_count = 0;
		$scope.message = null;

		var params = {
			keyword_qnh: !$scope.keyword ? null : $scope.keyword,
			limit: $scope.itemsPerPage,
			offset: pageno != null ? pageno : 1,
			page: JSON.stringify(url_arr),
		}
		$scope.loading = true;

		httpHandler.send({
			method: 'GET',
			url: urls + 'question/getQuestion',
			params: params
		}).then(
			function successCallbacks(response) {
				$scope.loading = false;
				$scope.data = response.data.data;
				$scope.total_count = response.data.count;
				$scope.message = response.data.message;
				$scope.curPage = pageno;
			},
			function errorCallback(response) {
				$scope.loading = false;
				$scope.data = response.data.data;
				$scope.total_count = response.data.count;
				$scope.curPage = pageno;
				$scope.message = !url_arr[3] ? response.data.message + ' <a class="url" href="' + urls + 'question/requestQuestion">Klik here</a>' : response.data.message;
			}
		);
	}

	if (!$scope.id_question) {
		$scope.getQuestion();
	}

	$scope.search = function () {
		$scope.keyword;
		$scope.getQuestion();
	}

	$scope.changeStatus = function (params) {
		var formData = new FormData();

		formData.append("id", params.id);
		formData.append("public_status", !params.public_status ? 0 : 1);

		httpHandler.send({
			url: urls + 'question/changeStatus',
			data: formData,
			method: 'POST',
			headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {});

	}

	$scope.addQuestion = function () {
		if (!$scope.question) {
			return Toast.fire({
				icon: "warning",
				title: 'Question Is Required!',
			});
		}

		Swal.fire({
			title: 'Loading...',
			allowEscapeKey: false,
			allowOutsideClick: false,
			showConfirmButton: false,
			imageUrl: urls + "assets/img/loadertsel.gif",
		});

		var formData = new FormData();

		formData.append("question", $scope.question);
		httpHandler.send({
			url: urls + 'question/requestQuestion',
			data: formData,
			method: 'POST',
			headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {
				Swal.close();
				Swal.fire({
					title: 'Success',
					text: response.data.message,
					icon: 'success',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#39edab",
					confirmButtonText: "Okey, otw list question!",
				}).then((result) => {
					if (result.value) {
						window.location.replace(urls + 'question');
					}
				});
			},
			function errorCallback(response) {
				Swal.close();
				Swal.fire({
					title: response.status == 500 ? 'Error' : 'Warning',
					text: response.data.message,
					icon: response.status == 500 ? 'error' : 'warning',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#fc544b",
					confirmButtonText: 'Back to list!',
				}).then((result) => {
					if (result.value) {
						window.location.replace(urls + 'question');
					}
				});
			});
	}

	$scope.getQuestionById = function () {
		Swal.fire({
			title: 'Loading...',
			allowEscapeKey: false,
			allowOutsideClick: false,
			showConfirmButton: false,
			imageUrl: urls + "assets/img/loadertsel.gif",
		});

		var param = {
			id: $scope.id_question,
		};

		httpHandler.send({
			method: 'GET',
			url: urls + 'question/prosesGetQuestionByID',
			params: param
		}).then(
			function successCallbacks(response) {
				Swal.close();
				$scope.question_by = response.data.data.name_question;
				$scope.question = response.data.data.question;

				var question_time = new Date(response.data.data.question_time);
				$scope.question_time = $filter('date')(question_time, "HH:mm dd-MM-yyyy");
				$scope.answer_by = response.data.data.name_answerer;
				$scope.answer = response.data.data.answer;
				var answer_time = !response.data.data.answer_time ? null : new Date(response.data.data.answer_time);
				$scope.answer_time = answer_time == null ? answer_time : $filter('date')(answer_time, "HH:mm dd-MM-yyyy");
				$scope.public_status = response.data.data.public_status == 1 ? true : false;
			},
			function errorCallback(response) {
				Swal.close();
				Swal.fire({
					title: response.status == 500 ? 'Error' : 'Warning',
					text: response.data.message,
					icon: response.status == 500 ? 'error' : 'warning',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#fc544b",
					confirmButtonText: 'Back to list!',
				}).then((result) => {
					if (result.value) {
						$scope.redirectList();
					}
				});
			}
		);
	}

	if ($scope.id_question) {
		$scope.getQuestionById();
	}

	$scope.saveAnswer = function () {
		if (!$scope.answer) {
			return Toast.fire({
				icon: "warning",
				title: 'Answer Is Required!',
			});
		}

		// Swal.fire({
		// 	title: 'Loading...',
		// 	allowEscapeKey: false,
		// 	allowOutsideClick: false,
		// 	showConfirmButton: false,
		// 	imageUrl: urls + "assets/img/loadertsel.gif",
		// });

		var formData = new FormData();

		formData.append("id", $scope.id_question);
		formData.append("answer", $scope.answer);
		formData.append("public_status", !$scope.public_status ? 0 : 1);

		httpHandler.send({
			url: urls + 'question/saveAnswer',
			data: formData,
			method: 'POST',
			headers: {
				'Content-Type': undefined
			}
		}).then(
			function successCallbacks(response) {
				Swal.close();
				Swal.fire({
					title: 'Success',
					text: response.data.message,
					icon: 'success',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#39edab",
					confirmButtonText: "Okey, otw list question!",
				}).then((result) => {
					if (result.value) {
						$scope.redirectList();
					}
				});
			},
			function errorCallback(response) {
				Swal.close();
				Swal.fire({
					title: response.status == 500 ? 'Error' : 'Warning',
					text: response.data.message,
					icon: response.status == 500 ? 'error' : 'warning',
					showCancelButton: false,
					allowEscapeKey: false,
					allowOutsideClick: false,
					confirmButtonColor: "#fc544b",
					confirmButtonText: 'Back to list!',
				}).then((result) => {
					if (result.value) {
						$scope.redirectList();
					}
				});
			});
	}

	$scope.redirectList = function () {
		window.location.replace($scope.url_list);
	}

}]);
