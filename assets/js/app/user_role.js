mainApp.controller('user_role', ['$scope', 'httpHandler', '$filter', function($scope, httpHandler, $filter){

	$scope.no = 1;
	$scope.itemsPerPage = 10;
	$scope.keyword = {};

    $scope.get_role = function (pageno) {
		$scope.pending = true;
		$scope.totalData = 0;
		$scope.itemData = {};

		if (pageno == 1)
			$scope.no = 1;
		else
			$scope.no = (pageno * $scope.itemsPerPage) - ($scope.itemsPerPage - 1);


		httpHandler.send({
			method: 'GET',
			url: mainUrl + 'user_role/get_role',
			params: {
				page: pageno,
				limit: $scope.itemsPerPage,
				keyword: $scope.keyword,
				column: true,
			}
		}).then(
			function successCallbacks(response) {
				$scope.pending = false;
				$scope.loader_listcategory = false;
				$scope.currentPage = response.data.page;
				$scope.itemData = response.data.data;

				$scope.totalData = response.data.total;
				$scope.last_update_data = response.data.last_update;

			},
			function errorCallback(response) {

			}
		);
	}

	$scope.get_role(1);

}]);