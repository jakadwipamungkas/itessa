<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * url in parameter.
 * @return void
 */
if (!function_exists('myUrlEncode')) {
    function myUrlEncode($string)
    {
        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D', '%E2%80%99');
        $replacements = "-";
        return str_replace('+', '-', str_replace($entities, $replacements, urlencode(strtolower($string))));
    }
}

if (!function_exists('setEncrypt')) {
    function setEncrypt($psswd)
    {

        // A higher "cost" is more secure but consumes more processing power
        $cost = 8;

        // Create a random salt
        $salt = strtr(base64_encode(bin2hex(random_bytes($cost))), '+', '.');

        // Prefix information about the hash so PHP knows how to verify it later.
        // "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
        $salt = sprintf("$2a$%02d$", $cost) . $salt;

        // Hash the password with the salt
        $hash = crypt($psswd, $salt);

        return $hash;
    }
}

if (!function_exists('setDecrypt')) {
    function setDecrypt($hash, $psswd)
    {

        if (hash_equals($hash, crypt($psswd, $hash))) {
            return true;
        } else
            return false;
    }
}


if (!function_exists('users_sessions')) {
    function users_sessions($param = false)
    {
        $ci = &get_instance();
        return !$param ? $ci->session->userdata('authTessa') : $ci->session->userdata('authTessa')[$param];
    }
}

if (!function_exists('update_session')) {
    function update_session($username)
    {
        $ci = &get_instance();
        $ci->load->model('UsersModel');


        $users = $ci->UsersModel->getUser($username);
        return $ci->session->set_userdata([
            'authTessa' => [
                'id'        => $users->id,
                'username'    => $users->username,
                'role_id'        => $users->role_id,
                'role_name'        => $users->role_name,
                'id_employee' => $users->id_employee,
                'full_name' => $users->full_name,
                'nick_name' => $users->nick_name,
                'date_of_birth' => $users->date_of_birth,
                'sex' => $users->sex,
                'marital_status' => $users->marital_status,
                'nationality' => $users->nationality,
                'citizen_number' => $users->citizen_number,
                'blood_type' => $users->blood_type,
                'religion' => $users->religion,
                'mobile_number' => $users->mobile_number,
                'email' => $users->email,
                'photo_profile' => !empty($users->photo_profile) ? $users->photo_profile : base_url() . 'assets/img/avatar/avatar-1.png',
                'citizen_file' => $users->citizen_file,
            ]
        ]);
    }
}

if (!function_exists('country')) {
    function country($param = false)
    {
        $ci = &get_instance();
        $ci->load->model('CountryModels');
        if (!$param) {
            return $ci->CountryModels->getCountry();
        }
        return $ci->CountryModels->getCountry($param);
    }
}

if (!function_exists('settingSocialMedia')) {
    function settingSocialMedia($param = false)
    {
        $ci = &get_instance();
        $ci->load->model('SocialMediaModels');
        if (!$param) {
            return $ci->SocialMediaModels->getSocialMedia();
        }
        return $ci->SocialMediaModels->getSocialMedia($param);
    }
}
