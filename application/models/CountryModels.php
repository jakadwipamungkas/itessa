<?php
class CountryModels extends CI_Model
{

    public function getCountry($params = false)
    {
        $this->db->select('id,continent,country,code');
        if (!empty($params)) {
            $this->db->like('country', $params);
        }
        $query = $this->db->get('ref_flag');
        if ($query->num_rows() > 1) {
            return $query->result_array();
        } elseif ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return [];
        }
    }
}
