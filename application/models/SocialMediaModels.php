<?php
class SocialMediaModels extends CI_Model
{

    public function getSocialMedia($params = false)
    {
        $this->db->select('id,name,button_class,icon_class,url');
        if (!empty($params)) {
            $this->db->like('name', $params);
        }
        $query = $this->db->get('ref_social_media_button');
        if ($query->num_rows() > 1) {
            return $query->result_array();
        } elseif ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return [];
        }
    }
}
