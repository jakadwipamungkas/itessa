<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tessa_Controller extends REST_Controller
{

    protected $data;

    function __construct()
    {
        parent::__construct();

        $this->data['new_sess'] = $this->session->userdata('authTessa');
        if (!empty($this->data['new_sess'])) {
            update_session($this->data['new_sess']['username']);
        }
        $this->data['users'] =  $this->session->userdata('authTessa');
        if ($this->uri->segment(1) != 'login' && $this->uri->segment(1) != 'verify' && $this->uri->segment(1) != 'forgot') {
            if (!$this->data['users'])
                redirect(base_url('login'));
        } elseif ($this->data['users'] != null) {
            redirect(base_url());
        }
    }
}
