<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends Tessa_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('employee_model');
	}

	public function saveTemporary_get()
	{
		$data = $this->get();
		$data['role_name'] = users_sessions('role_name');

		$return = $this->employee_model->saveTemporary($data);
		$this->response($return);
	}

	public function detail_get()
	{
		$this->data['title'] = 'Detail Employee ~ iTessa';
		$this->data['version'] = $this->uri->segment(1);
		$this->data['id_employee'] = !empty($this->get('id_employee')) ? encrypt_url($this->get('id_employee')) : encrypt_url(users_sessions('id_employee'));
		$this->data['image'] = base_url('assets/img/question.png');
		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
			'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
			'assets/plugins/bootstrap-social/bootstrap-social.css',
			'assets/plugins/flag-icon-css/assets/docs.css',
			'assets/plugins/flag-icon-css/css/flag-icon.css',
			'assets/plugins/fancybox/dist/jquery.fancybox.min.css',
			'assets/plugins/select2/dist/css/select2.min.css'
		];

		$this->data['js'] = array(
			'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
			'assets/plugins/fancybox/dist/jquery.fancybox.min.js',
			'assets/plugins/select2/dist/js/select2.min.js',
			'assets/js/app/employee.js?' . rand()
		);

		$this->template->load($this->data, null, 'index');
	}


	public function getDetail_get()
	{
		$params = $this->get();
		$return = $this->employee_model->getDetail($params);
		$this->response($return, $return['status']);
	}

	public function getUpdate_get()
	{
		$this->data['title'] = 'Change Data Employee ~ iTessa';
		$this->data['version'] = $this->uri->segment(1);
		$this->data['id_employee'] = !empty($this->get('id_employee')) ? $this->get('id_employee') : users_sessions('id_employee');
		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
			'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
			'assets/plugins/bootstrap-social/bootstrap-social.css',
			'assets/plugins/flag-icon-css/assets/docs.css',
			'assets/plugins/flag-icon-css/css/flag-icon.css',
			'assets/plugins/fancybox/dist/jquery.fancybox.min.css',
			'assets/plugins/select2/dist/css/select2.min.css'
		];

		$this->data['js'] = array(
			'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
			'assets/plugins/fancybox/dist/jquery.fancybox.min.js',
			'assets/plugins/select2/dist/js/select2.min.js',
			'assets/js/app/employee.js?' . rand()
		);

		$this->template->load($this->data, null, 'form_update');
	}


	public function getCountry_get()
	{
		$this->response(country());
	}

	public function getSocialMedia_get()
	{
		$this->response(settingSocialMedia());
	}
}
