<style>
	.img-custom {
		height: 25%;
		width: 25%;
	}
</style>
<div class="main-content" ng-controller="employee" data-id_employee="<?= $id_employee ?>">
	<section class="section">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-12 col-sm-9 col-md-10 col-lg-10">
								<h6>
									<i class="fas fa-user-tie"></i> Detail Employee <span ng-bind="'('+status_user+')'"></span>
								</h6>
							</div>
							<div class="col-sm-3 col-md-2 col-lg-2 text-right hidden-attrs-sm">
								<a href="{{url_edited}}" class="btn btn-danger btn-sm" title="Change Data Employee"><i class="fas fa-user-edit"></i></a>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<ul class="nav nav-tabs justify-content-left" id="myTab6" role="tablist">
							<li class="nav-item">
								<a class="nav-link active text-center" id="profile" data-toggle="tab" href="#profile6" role="tab" aria-controls="profile" aria-selected="true">
									<span><i class="fas fa-id-card"></i></span> Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="education" data-toggle="tab" href="#education6" role="tab" aria-controls="education" aria-selected="false">
									<span><i class="fas fa-user-graduate"></i></span> Education</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="family-tab6" data-toggle="tab" href="#family6" role="tab" aria-controls="family" aria-selected="false">
									<span><i class="fas fa-users"></i></span> Family</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="language-tab6" data-toggle="tab" href="#language6" role="tab" aria-controls="language" aria-selected="false">
									<span><i class="fas fa-language"></i></span> Language</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="community-tab6" data-toggle="tab" href="#community6" role="tab" aria-controls="community" aria-selected="false">
									<span><i class="fas fa-id-badge"></i></span> Community</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="workExperience-tab6" data-toggle="tab" href="#workExperience6" role="tab" aria-controls="workExperience" aria-selected="false">
									<span><i class="fas fa-business-time"></i></span> Work Experience</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="medical-tab6" data-toggle="tab" href="#medical6" role="tab" aria-controls="medical" aria-selected="false">
									<span><i class="fas fa-book-medical"></i></span> Medical Record</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="references-tab6" data-toggle="tab" href="#references6" role="tab" aria-controls="references" aria-selected="false">
									<span><i class="fas fa-people-arrows"></i></span> Employment References</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-center" id="award-tab6" data-toggle="tab" href="#award6" role="tab" aria-controls="award" aria-selected="false">
									<span><i class="fas fa-trophy"></i></span> Award</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent6">
							<div class="tab-pane fade show active" id="profile6" role="tabpanel" aria-labelledby="profile-tab6">
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-12">
										<div class="card author-box">
											<div class="card-body">
												<div class="author-box-left">
													<img alt="image" ng-src="{{personal.photo_profile}}" class="rounded-circle author-box-picture">
													<div class="clearfix"></div>
												</div>
												<div class="author-box-details">
													<div class="col-12 col-md-12 col-lg-12">
														<div class="author-box-name">
															<a class="url" ng-bind-html="personal.full_name + ' ' +personal.sex"></a>
														</div>
													</div>
													<div class="w-100 d-sm-none"></div>
													<div class="author-box-description">
														<div class="row">
															<div class="col-12 col-md-6 col-lg-6">
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Email:</h6>
																	<p ng-bind="personal.email"></p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Mobile Phone:</h6>
																	<p ng-bind="personal.mobile_number"></p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Citizen Number:</h6>
																	<p ng-bind="personal.citizen_number"></p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Date & Place Birth:</h6>
																	<p ng-bind="personal.date_of_place+', '+ personal.date_of_birth">
																	</p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Religion:</h6>
																	<p ng-bind="personal.religion|capitalize"></p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Nationality:</h6>
																	<p ng-bind="personal.nationality|capitalize"></p>
																</div>
															</div>
															<div class=" col-12 col-md-6 col-lg-6">
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Marital Status:</h6>
																	<p ng-bind="personal.marital_status"></p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Height/Weight:</h6>
																	<p ng-bind="personal.height+'/'+personal.weight">
																	</p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Blood Type:</h6>
																	<p ng-bind="personal.blood_type|capitalize"></p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Current Address:</h6>
																	<p ng-bind="personal.current_address + ' ' + personal.current_city_address |uppercase">
																	</p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<h6 class="label-inverse">Formal Address:</h6>
																	<p ng-bind="personal.formal_address+' '+personal.formal_city_address |uppercase">
																	</p>
																</div>
																<div class="col-12 col-md-12 col-lg-12">
																	<div class="gallery">
																		<h6 class="label-inverse">Document Personal:
																		</h6>
																		<a ng-show="personal.citizen_file != null" class="url m-1" href="{{personal.citizen_file}}" target="_blank" title="show document citizen">
																			<i ng-show="personal.citizen_type_file == 'photo'" class="fas fa-file-image fa-2x"></i>
																			<i class="fas fa-file-pdf fa-2x" ng-show="personal.citizen_type_file !='photo'"></i>
																		</a>
																		<span ng-show="personal.citizen_file == null" title="citizen file empty!"><i class="fas fa-file fa-2x"></i></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-12 col-md-12 col-lg-12">
														<a ng-href="{{media_social.url}}{{personal.social_media_accounts}}" class="btn btn-social-icon mr-1 {{media_social.button_class}}" ng-bind-html="media_social.icon_class">
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="education6" role="tabpanel" aria-labelledby="education-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities" ng-show="education !=null">
												<div ng-repeat="value_education in education" class="activity">
													<div class="activity-icon bg-danger text-white shadow-danger">
														<i class="fas fa-graduation-cap text-25"></i>
													</div>
													<div class="activity-detail">
														<div>
															<span class="text-16 url text-primary" ng-bind="value_education.degree_education|uppercase"></span>
															<span ng-show="value_education.degree_education != null && value_education.degree_name != null" class="bullet"></span>
															<span class="text-16 url text-primary" ng-bind="value_education.degree_name+'('+value_education.degree_graduation_year+')'|uppercase"></span>
														</div>
														<p>Education Level:
															{{!value_education.last_education?'-':value_education.last_education}}
														</p>
														<p>Degree Major:
															{{!value_education.degree_major?'-':value_education.degree_major|capitalize}}
														</p>
														<p>Degree GPA:
															{{!value_education.degree_gpa?'-':value_education.degree_gpa}}
														</p>
														<div class="gallery">
															<a ng-show="value_education.document_certificate != null" class="url m-1" href="{{value_education.document_certificate}}" target="_blank" title="show certificate">
																<i ng-show="value_education.type_certificate == 'photo'" class="fas fa-file-image fa-2x"></i>
																<i class="fas fa-file-pdf fa-2x" ng-show="value_education.type_certificate !='photo'"></i>
															</a>
															<span ng-show="value_education.document_certificate == null"><i class="
																fas fa-file fa-2x"></i></span>
														</div>
													</div>
												</div>
											</div>
											<div class="text-center" ng-show="education == null">
												<img src="<?= $image; ?>" class="img-custom">
												<p ng-bind="'sorry Education empty, please complete your Education!'">
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="family6" role="tabpanel" aria-labelledby="family-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities">
												<div ng-repeat="value_family in family" class="activity">
													<div class="activity-icon bg-danger text-white shadow-danger">
														<i ng-show="value_family.gender == 'Male'" class="text-25 fas fa-male"></i>
														<i ng-show="value_family.gender != 'Male'" class="text-25 fas fa-female"></i>
													</div>
													<div class="activity-detail">
														<div>
															<span class="url text-primary" ng-bind="value_family.family_type|capitalize"></span>
															<span class="bullet"></span>
															<a class="url text-primary" ng-bind="value_family.full_name|capitalize"></a>
														</div>
														<p>Citizen Number:
															{{!value_family.citizen_number?'-':value_family.citizen_number}}
														</p>
														<p>Date And Place:
															{{value_family.place_of_birth + ', '+value_family.date_of_birth}}
														</p>
														<p>Latest Education:
															{{!value_family.latest_education?'-':value_family.latest_education}}
														</p>
														<p>Occaption Title:
															{{!value_family.occaption_title?'-':value_family.occaption_title}}
														</p>
														<p>Company: {{!value_family.company?'-':value_family.company}}
														</p>
														<p>Company Address:
															{{!value_family.company_address?'-':value_family.company_address}}
														</p>
														<p>Contact Number:
															{{!value_family.contact_number?'-':value_family.contact_number}}
														</p>
														<!-- <p>Height/Weight: {{!value_family.company_addres?'-':value_family.company_addres}}</p> -->
														<p>Blood Type:
															{{!value_family.blood_type?'-':value_family.blood_type}}
														</p>
														<p>Religion:
															{{!value_family.religion?'-':value_family.religion}}
														</p>
														<p>Current Address:
															{{!value_family.current_address?'-':value_family.current_address+' '+value_family.current_city}}
														</p>
														<div class="gallery">
															<h6 class="label-inverse">Document Person:</h6>
															<a ng-show="value_family.family_card" class="url" href="{{value_family.family_card}}" target="_blank" title="show document card family">
																<i ng-show="value_family.family_card_type == 'photo'" class="fas fa-file-image fa-2x"></i>
																<i class="fas fa-file-pdf fa-2x" ng-show="value_family.family_card_type !='photo'"></i>
															</a>
															<span ng-show="value_family.family_card == null" title="family card empty!"><i class="fas fa-file fa-2x"></i></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="language6" role="tabpanel" aria-labelledby="language-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities" ng-show="language != null">
												<div class="activity" ng-repeat="value_language in language">
													<div class="col-sm-2 col-xs-3">
														<div class="flag-wrapper">
															<div class="flag flag-icon-background {{value_language.icon_language}}">
															</div>
														</div>
													</div>
													<div class="activity-detail">
														<div>
															<span class="url text-primary" ng-bind="value_language.language|capitalize"></span>
															<span ng-show="value_language.language_test_institute !=null" class="bullet"></span>
															<a class="url text-primary" ng-bind="value_language.language_test_institute"></a>
														</div>
														<p>Test Score:
															{{!value_language.test_score?'-':value_language.test_score}}
														</p>
														<a class="url m-1" href="{{!value_language.language_certificate?'#':value_language.language_certificate}}" target="_blank" title="show certificate">
															<i ng-show="value_education.type_certificate == 'photo' && value_language.language_certificate" class="fas fa-file-image fa-2x"></i>
															<i class="fas fa-file-pdf fa-2x" ng-show="value_education.type_certificate !='photo' && value_language.language_certificate"></i>
														</a>
													</div>
												</div>
											</div>
											<div class="text-center" ng-show="language == null">
												<img src="<?= $image; ?>" class="img-custom">
												<p ng-bind="'sorry language empty, please complete your language!'"></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="community6" role="tabpanel" aria-labelledby="community-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities" ng-show="membership != null">
												<div class="activity" ng-repeat="value_membership in membership">
													<div class="activity-icon bg-danger text-white shadow-danger">
														<i class="fas fa-users" style="font-size: 20px;"></i>
													</div>
													<div class="activity-detail">
														<div>
															<span class="url text-primary" ng-bind="value_membership.professional_membership_association"></span>
															<span class="bullet"></span>
															<a class="url" ng-bind="value_membership.roles_to_association"></a>
														</div>
														<p>Member Ship Time: {{value_membership.membership_time}}</p>
														<div class="gallery">
															<a class="url m-1" href="{{value_membership.member_doc}}" target="_blank" title="show membership document">
																<i ng-show="value_membership.type_doc == 'photo'" class="fas fa-file-image fa-2x"></i>
																<i class="fas fa-file-pdf fa-2x" ng-show="value_membership.type_doc !='photo'"></i>
															</a>
														</div>
													</div>
												</div>
											</div>
											<div class="text-center" ng-show="membership == null">
												<img src="<?= $image; ?>" class="img-custom">
												<p ng-bind="'sorry comunity empty, please complete your comunity!'"></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="workExperience6" role="tabpanel" aria-labelledby="workExperience-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities" ng-show="experiences != null">
												<div class="activity" ng-repeat="value_experience in experiences">
													<div class="activity-icon bg-danger text-white shadow-danger">
														<i class="fas fa-briefcase" style="font-size: 20px;"></i>
													</div>
													<div class="activity-detail">
														<div>
															<span class="url text-primary" ng-bind="value_experience.last_company"></span>
															<span class="bullet"></span>
															<a class="url" ng-bind="value_experience.work_type"></a>
														</div>
														<p>Total Time Experience:
															{{value_experience.total_time_experience}}
														</p>
														<p>Last Position: {{value_experience.last_position_title}}</p>
														<p>Last Roles: {{value_experience.last_roles}}</p>
														<p>Last Working Tenur: {{value_experience.last_working_tenure}}
														</p>
														<p>Reason Leaving: {{value_experience.reason_for_leaving}}</p>
														<div class="gallery">
															<a ng-show="value_experience.salary_slip_doc != null" class="url m-1" href="{{value_experience.salary_slip_doc}}" target="_blank" title="show slip document">
																<i ng-show="value_experience.slip_type == 'photo'" class="fas fa-file-image fa-2x"></i>
																<i class="fas fa-file-pdf fa-2x" ng-show="value_experience.slip_type !='photo'"></i>
															</a>
															<span ng-show="value_experience.salary_slip_doc == null" title="salary slip document empty!"><i class="fas fa-file fa-2x"></i></span>
															<a ng-show="value_experience.certificate !=null" class="url m-1" href="{{value_experience.certificate}}" target="_blank" title="show ceritiface document">
																<i ng-show="value_experience.type_certificate == 'photo'" class="fas fa-file-image fa-2x"></i>
																<i class="fas fa-file-pdf fa-2x" ng-show="value_experience.type_certificate !='photo'"></i>
															</a>
															<span ng-show="value_experience.certificate == null" title="certificate empty!"><i class="fas fa-file fa-2x"></i></span>
														</div>
													</div>
												</div>
											</div>
											<div class="text-center" ng-show="experiences == null">
												<img src="<?= $image; ?>" class="img-custom">
												<p ng-bind="'sorry work experience empty, please complete your work experience!'">
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="medical6" role="tabpanel" aria-labelledby="medical-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="list-group" ng-show="medical != null">
										<a href="#" class="list-group-item list-group-item-action flex-column align-items-start {{medical.serious_disease == '0'?'':'active'}}">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1">Serious Disease</h5>
												<small ng-bind="medical.serious_disease == '0'?'No':'Yes'"></small>
											</div>
											<p class="mb-1" ng-bind="!medical.desc_disease?'Empty Serious Desease':medical.desc_disease">
											</p>
										</a>
										<a href="#" class="list-group-item list-group-item-action flex-column align-items-start {{medical.serious_accident == '0'?'':'active'}}">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1">Serious Accident</h5>
												<small ng-bind="medical.serious_accident == '0'?'No':'Yes'"></small>
											</div>
											<p class="mb-1" ng-bind="!medical.desc_accident?'Empty Serious Accident':medical.desc_accident">
											</p>
										</a>
										<a href="#" class="list-group-item list-group-item-action flex-column align-items-start {{medical.color_blindness == '0'?'':'active'}}">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1">Color Blindness</h5>
												<small ng-bind="medical.color_blindness == '0'?'No':'Yes'"></small>
											</div>
										</a>
										<a href="#" class="list-group-item list-group-item-action flex-column align-items-start {{medical.other_medical_issue == null?'':'active'}}">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1">Other Medical Issue</h5>
												<small ng-bind="medical.other_medical_issue == null?'':'yes'"></small>
											</div>
											<p class="mb-1" ng-bind="!medical.other_medical_issue?'Empty Serious Accident':medical.other_medical_issue">
											</p>
										</a>
									</div>
									<div class="text-center" ng-show="medical == null">
										<img src="<?= $image; ?>" class="img-custom">
										<p ng-bind="'sorry medical record empty, please complete your medical record!'">
										</p>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="references6" role="tabpanel" aria-labelledby="references-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities" ng-show="references != null">
												<div class="activity" ng-repeat="value_reference in references">
													<div class="activity-icon bg-danger text-white shadow-danger">
														<i class="fas fa-user-tie" style="font-size: 20px;"></i>
													</div>
													<div class="activity-detail">
														<div>
															<span class="url text-primary" ng-bind="value_reference.name"></span>
															<span ng-show="value_reference.name != null && value_reference.job_title != null" class="bullet"></span>
															<a class="url" ng-bind="value_reference.job_title"></a>
														</div>
														<p>Company:
															{{!value_reference.company?'-':value_reference.company|capitalize}}
														</p>
														<p>Affilition:
															{{!value_reference.affilition?'-':value_reference.affilition|capitalize}}
														</p>
														<p>Contact
															Number:
															{{!value_reference.contact_number?'-':value_reference.contact_number}}
														</p>
													</div>
												</div>
											</div>
											<div class="text-center" ng-show="references == null">
												<img src="<?= $image; ?>" class="img-custom">
												<p ng-bind="'sorry references empty, please complete your references!'">
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="award6" role="tabpanel" aria-labelledby="award-tab6">
								<div class="col-12 col-md-12 col-lg-12 mt-3">
									<div class="row">
										<div class="col-12 col-md-12">
											<div class="activities" ng-show="award != null">
												<div class="activity" ng-repeat="value_award in award">
													<div class="activity-icon bg-danger text-white shadow-danger">
														<i class="fas fa-trophy" style="font-size: 20px;"></i>
													</div>
													<div class="activity-detail">
														<div>
															<span class="url text-primary" ng-bind="value_award.type_award"></span>
															<span class="bullet"></span>
															<a class="url" ng-bind="value_award.award_name"></a>
														</div>
														<p>Year of award: {{value_award.year_of_award}}</p>
														<p>Roles to the award: {{value_award.roles_to_the_award}}</p>
													</div>
												</div>
											</div>
											<div class="text-center" ng-show="award == null">
												<img src="<?= $image; ?>" class="img-custom">
												<p ng-bind="'sorry awards empty, please complete your awards!'"></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<a class="float hide-float-button" id="menu-share" title="show tools">
							<i class="fas fa-caret-down my-float text-white"></i>
						</a>
						<ul class="float-right-btn hide-float-button">
							<li>
								<a href="{{url_edited}}" title="Change Data Employee">
									<i class="fas fa-user-edit my-float text-white"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>