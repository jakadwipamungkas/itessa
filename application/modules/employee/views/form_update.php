<div class="main-content" ng-controller="employee" data-id_employee="<?= $id_employee ?>">
	<section class="section">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12">
								<h6>
									<i class="fas fa-user-tie"></i> Update Employee
								</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-status p-3" ng-show="card_personal">
						<form ng-submit="savePersonal()">
							<div class="section-title mt-0">Personal Data</div>
							<div class="row">
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="photo_profile">Photo Profile</label>
										<div class="input-group">
											<div class="input-group-append">
												<a class="btn {{photo_profile != null ?'btn-danger':'btn-secondary btn-disabled'}}"
													title="show document citizen" href="{{photo_profile != null?photo_profile:'#'}}" data-fancybox
													data-caption="{{photo_profile_name !=null?photo_profile_name:'photo empty!'}}">
													<i ng-show="photo_profile != null" class="fas fa-file-image fa-2x text-white"></i>
													<i ng-show="photo_profile == null" class="fas fa-file fa-2x text-white"></i>
												</a>
											</div>
											<div class="custom-file">
												<input required type="file" ngf-select="" ng-model="file_ba_kehilangan"
													name="file_ba_kehilangan" ngf-accept="'.pdf,.PDF,.jpg,.Jpg,.JPEG,.jpeg,.PNG,.png'"
													class="custom-file-input" id="photo_profile">
												<label class="custom-file-label"
													ng-bind="photo_profile_name != null?photo_profile_name:'choose file'"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="doc_citizen">Document Citizen</label>
										<div class="input-group">
											<div class="input-group-append">
												<a ng-show="citizen_type_file == 'document'||citizen_type_file == null" href="{{citizen_file}}"
													target="{{citizen_type_file == 'document'?'_blank':null}}"
													class="btn {{citizen_file != null ?'btn-danger':'btn-secondary btn-disabled'}}" type="button"
													title="{{citizen_file !=null && citizen_type_file != null ? 'show document citizen':'citizen empty!'}}">
													<i ng-show="citizen_type_file == 'document' && citizen_file != null"
														class="fas fa-file-pdf fa-2x text-white"></i>
													<i ng-show="citizen_file == null" class="fas fa-file fa-2x text-white"></i>
												</a>
												<a ng-show="citizen_type_file == 'photo' && citizen_file != null"
													class="btn {{citizen_file != null ?'btn-danger':'btn-secondary btn-disabled'}}"
													title="show document citizen" href="{{citizen_file}}" data-fancybox
													data-caption="{{citizen_file_name}}">
													<i class="fas fa-file-image fa-2x text-white"></i>
												</a>
											</div>
											<div class="custom-file">
												<input type="file" ngf-select="" ng-model="citizen_file" name="citizen_file"
													ngf-accept="'.pdf,.PDF,.jpg,.Jpg,.JPEG,.jpeg,.PNG,.png'" class="custom-file-input"
													id="photo_profile">
												<label class="custom-file-label"
													ng-bind="citizen_file_name != null?citizen_file_name:'choose file'"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="nik">NIK</label>
										<input type="text" class="form-control" id="nik" name="nik" ng-model="nik" disabled>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="email_ldap">Email Ldap</label>
										<input type="text" class="form-control" id="email_ldap" name="email_ldap" ng-model="email_ldap"
											disabled>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="full_name">Full Name</label>
										<input type="text" class="form-control" id="full_name" name="full_name" ng-model="full_name"
											placeholder="Input Your Full Name">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="nick_name">Nick Name</label>
										<input type="text" class="form-control" id="nick_name" name="nick_name" ng-model="nick_name"
											placeholder="Input Your Nick Name">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="date_of_place">Birth Of Place</label>
										<input type="text" class="form-control" id="date_of_place" name="date_of_place"
											ng-model="date_of_place" placeholder="Example: Tokyo">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="date_of_birth">Birth Of Date</label>
										<div class="input-group">
											<input type="text" class="form-control autoclose-datepicker" readonly id="date_of_birth"
												name="date_of_birth" ng-model="date_of_birth"
												placeholder="Click Button Calendar To Select Date">
											<div class="input-group-append">
												<button class="btn btn-danger btn-disabled-danger" ng-model="date_of_birth"
													ng-click="changeBirth()" type="button"><i class="fas fa-calendar-alt fa-2x"></i></button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="sex">Gender</label>
										<div class="selectgroup w-100">
											<label class="selectgroup-item" title="male">
												<input type="radio" class="selectgroup-input" ng-value="1" ng-model="sex">
												<span class="selectgroup-button selectgroup-button-icon"><i
														class="fas fa-male fa-3x"></i></span>
											</label>
											<label class="selectgroup-item" title="female">
												<input type="radio" name="sex" ng-value="2" ng-model="sex" class="selectgroup-input">
												<span class="selectgroup-button selectgroup-button-icon"><i
														class="fas fa-female fa-3x"></i></span>
											</label>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="marital_status">Marital Status</label>
										<div class="selectgroup w-100">
											<label class="selectgroup-item" title="single">
												<input type="radio" ng-value="1" ng-model="marital_status" class="selectgroup-input">
												<span class="selectgroup-button selectgroup-button-icon"><i
														class="fas fa-user-times"></i></span>
											</label>
											<label class="selectgroup-item" title="maried">
												<input type="radio" ng-value="2" ng-model="marital_status" class="selectgroup-input">
												<span class="selectgroup-button selectgroup-button-icon"><i
														class="fas fa-restroom fa-3x"></i></span>
											</label>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="nationality">Nationality</label>
										<select class="form-control live-search" data-live-search="true" name="nationality" id="nationality"
											ng-model="nationality">
											<option value="" selected disabled>Choose Nationality</option>
											<option ng-repeat="value_country in dataCountry" value="{{value_country.country}}"
												ng-bind="value_country.country|capitalize"></option>
										</select>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="citizen_number">Citizen Number</label>
										<input type="number" min="0" minlength="10" maxlength="30" class="form-control number-noslide"
											id="citizen_number" name="citizen_number" ng-model="citizen_number"
											placeholder="Example: 3171xxxxxx">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="height">Height</label>
										<div class="input-group">
											<input type="number" min="1" class="form-control" id="height" name="height" ng-model="height"
												placeholder="Insert Your Height">
											<div class="input-group-append">
												<button class="btn btn-danger" style="cursor: text;" type="button">Cm</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="weight">Weight</label>
										<div class="input-group">
											<input type="number" min="1" class="form-control" id="weight" name="weight" ng-model="weight"
												placeholder="Insert Your Weight">
											<div class="input-group-append">
												<button class="btn btn-danger" style="cursor: text;" type="button">Kg</button>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="blood_type">Blood Type</label>
										<select class="form-control" name="blood_type" id="blood_type" ng-model="blood_type">
											<option value="" selected disabled>Choose Blood Type</option>
											<option value="a">A</option>
											<option value="b">B</option>
											<option value="ab">AB</option>
											<option value="o">O</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="religion">Religion</label>
										<select class="form-control" name="religion" id="religion" ng-model="religion">
											<option value="" selected disabled>Choose Religion</option>
											<option value="other">Other</option>
											<option value="islam">Islam</option>
											<option value="kristen">Kristen</option>
											<option value="khatolik">Khatolik</option>
											<option value="buddha">Buddha</option>
											<option value="hindu">Hindu</option>
											<option value="konghucu">Konghucu</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="mobile_number">Mobile Number</label>
										<input type="number" min="0" class="form-control number-noslide" id="mobile_number"
											name="mobile_number" ng-model="mobile_number" placeholder="Insert Your Mobile Number">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="email">Email</label>
										<input type="email" class="form-control phone-number" id="email" name="email" ng-model="email"
											placeholder="Insert Your Email">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="social_media_app">Social Media App</label>
										<select class="form-control" name="social_media_app" id="social_media_app"
											ng-model="social_media_app">
											<option value="" selected disabled>Choose App Name</option>
											<option ng-repeat="value_social in dataSocialMedia" value="{{value_social.name}}"
												ng-bind="value_social.name|capitalize">
											</option>
										</select>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="social_media_accounts">Account Name</label>
										<input type="text" class="form-control" id="social_media_accounts" name="social_media_accounts"
											ng-model="social_media_accounts" placeholder="Input Your Full Name">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-8">
									<div class="form-group">
										<label for="current_address">Current Address</label>
										<input type="text" class="form-control" id="current_address" name="current_address"
											ng-model="current_address" placeholder="Input Your Current Address">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-4">
									<div class="form-group">
										<label for="current_city_address">Current City</label>
										<input type="text" class="form-control" id="current_city_address" name="current_city_address"
											ng-model="current_city_address" placeholder="Input Your Current City">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-8">
									<div class="form-group">
										<label for="formal_address">Formal Address</label>
										<input type="text" class="form-control" id="formal_address" name="formal_address"
											ng-model="formal_address" placeholder="Input Your Formal Address">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-4">
									<div class="form-group">
										<label for="formal_city_address">Formal City</label>
										<input type="text" class="form-control" id="formal_city_address" name="formal_city_address"
											ng-model="formal_city_address" placeholder="Input Your Formal City">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-12 text-right">
									<div class="form-group">
										<button class="btn btn-warning btn-lg" type="button">Cancel</button>
										<button class="btn btn-danger btn-lg" type="submit">Save</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
