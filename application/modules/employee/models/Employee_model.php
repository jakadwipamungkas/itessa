<?php
class Employee_model extends CI_Model
{
	// assets select employee
	public function selecArr($params)
	{
		if ($params == 'employee_personal_data') {
			return [
				'employee_personal_data.*',
				'users.nik',
				'users.email_ldap',
				'users.status_user',
				'tbl_profile.url_document photo_profile',
				'tbl_profile.file_name photo_profile_name',
				'tbl_file_citizen.url_document citizen_file',
				'tbl_file_citizen.file_name citizen_file_name',
				'tbl_file_citizen.type_file citizen_type_file',
			];
		} elseif ($params == 'employee_educational_background') {
			return [
				'employee_educational_background.*',
				'tbl_certificate.url_document document_certificate',
				'tbl_certificate.type_file type_certificate',
			];
		} elseif ($params == 'employee_family') {
			return [
				'employee_family.*',
				'tbl_family_card.url_document family_card',
				'tbl_family_card.type_file family_card_type',
			];
		} elseif ($params == 'employee_language') {
			return [
				'employee_language.*',
				'certificate_language.url_document language_certificate',
				'certificate_language.type_file type_certificate',
			];
		} elseif ($params == 'employee_professional_membership') {
			return [
				'epm.*',
				'membership_doc.url_document member_doc',
				'membership_doc.type_file type_doc',

			];
		} elseif ($params == 'employee_work_experience') {
			return [
				'ewe.*',
				'certificate.url_document certificate',
				'certificate.type_file type_certificate',
				'salary_slip.url_document salary_slip_doc',
				'salary_slip.type_file slip_type',
			];
		} elseif ($params == 'employee_medical_record') {
			return [
				'id',
				'id_employee',
				'serious_disease',
				'desc_disease',
				'serious_accident',
				'desc_accident',
				'color_blindness',
				'other_medical_issue',
			];
		} elseif ($params == 'employee_references') {
			return [
				'id',
				'id_employee',
				'name',
				'job_title',
				'company',
				'affilition',
				'contact_number'
			];
		} elseif ($params == 'employee_award') {
			return [
				'employee_award.*',
				'award_doc.url_document award_document',
				'award_doc.type_file award_type',
			];
		}
	}
	// assets select employee

	public function saveTemporary($params)
	{
		$this->db->select($this->selecArr('employee_personal_data'))
			->from('employee_personal_data')
			->join('users', 'users.id = employee_personal_data.id_users', 'left')
			->join("ref_file_uploaded as tbl_profile", "tbl_profile.id = employee_personal_data.id_photo_profile", "left")
			->join("ref_file_uploaded as tbl_file_citizen", "tbl_file_citizen.id = employee_personal_data.id_photo_citizen_number", "left")
			->where('employee_personal_data.id', decrypt_url($params['id']));
		$get_personal_data = $this->db->get();

		if ($get_personal_data->first_row()->status_user == 1 || $get_personal_data->first_row()->status_user == 2 || $get_personal_data->first_row()->status_user == 3) {
			return [
				'message' => 'data not available to create temporary!',
				'status' => 200,
			];
		}

		$this->db->where('id', $get_personal_data->first_row()->id_users)->update('users', [
			'status_user' => 1,
			'updated_at' => date('Y-m-d H:i:s')
		]);

		foreach ($get_personal_data->result_array() as $key => $value) {
			$dt_personal = [
				'id_personal' => $value['id'],
				'id_users' => $value['id_users'],
				'id_photo_profile' => $value['id_photo_profile'],
				'id_photo_citizen_number' => $value['id_photo_citizen_number'],
				'full_name' => $value['full_name'],
				'nick_name' => $value['nick_name'],
				'date_of_birth' => $value['date_of_birth'],
				'date_of_place' => $value['date_of_place'],
				'sex' => $value['sex'],
				'marital_status' => $value['marital_status'],
				'nationality' => $value['nationality'],
				'citizen_number' => $value['citizen_number'],
				'height' => $value['height'],
				'weight' => $value['weight'],
				'blood_type' => $value['blood_type'],
				'religion' => $value['religion'],
				'mobile_number' => $value['mobile_number'],
				'email' => $value['email'],
				'social_media_app' => $value['social_media_app'],
				'social_media_accounts' => $value['social_media_accounts'],
				'current_address' => $value['current_address'],
				'current_city_address' => $value['current_city_address'],
				'formal_address' => $value['formal_address'],
				'formal_city_address' => $value['formal_city_address'],
				'join_date' => $value['join_date'],
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];
		}

		$this->db->insert('temporary_employee_personal_data', $dt_personal);

		$this->db->select($this->selecArr('employee_educational_background'))
			->from('employee_educational_background')
			->join("ref_file_uploaded as tbl_certificate", "tbl_certificate.id = employee_educational_background.id_certificate", "left")
			->where('employee_educational_background.id_employee', decrypt_url($params['id']));
		$get_education = $this->db->get()->result_array();
		if (!empty($get_education)) {
			foreach ($get_education as $value) {
				$dt_education[] = [
					'id_educational' => $value['id'],
					'id_employee' => $value['id_employee'],
					'id_certificate' => $value['id_certificate'],
					'last_education' => $value['last_education'],
					'degree_name' => $value['degree_name'],
					'degree_education' => $value['degree_education'],
					'degree_major' => $value['degree_major'],
					'degree_gpa' => $value['degree_gpa'],
					'degree_graduation_year' => $value['degree_graduation_year'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}
			$this->db->insert_batch('temporary_employee_educational_background', $dt_education);
		}

		$this->db->select($this->selecArr('employee_family'))
			->from('employee_family')
			->join("ref_file_uploaded as tbl_family_card", "tbl_family_card.id = employee_family.id_family_card", "left")
			->where('employee_family.id_employee', decrypt_url($params['id']));
		$get_family = $this->db->get()->result_array();
		if (!empty($get_family)) {
			foreach ($get_family as $key => $value) {
				$dt_family[] = [
					'id_family' => $value['id'],
					'id_employee' => $value['id_employee'],
					'id_family_card' => $value['id_family_card'],
					'family_type' => $value['family_type'],
					'citizen_number' => $value['citizen_number'],
					'full_name' => $value['full_name'],
					'place_of_birth' => $value['place_of_birth'],
					'date_of_birth' => $value['date_of_birth'],
					'latest_education' => $value['latest_education'],
					'gender' => $value['gender'],
					'occupation_title' => $value['occupation_title'],
					'company' => $value['company'],
					'company_address' => $value['company_address'],
					'contact_number' => $value['contact_number'],
					'height' => $value['height'],
					'weight' => $value['weight'],
					'blood_type' => $value['blood_type'],
					'religion' => $value['religion'],
					'current_address' => $value['current_address'],
					'current_city' => $value['current_city'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}

			$this->db->insert_batch('temporary_employee_family', $dt_family);
		}

		$this->db->select($this->selecArr('employee_language'))
			->from('employee_language')
			->join("ref_file_uploaded as certificate_language", "certificate_language.id = employee_language.id_language_certificate", "left")
			->where('employee_language.id_employee', decrypt_url($params['id']));
		$get_language = $this->db->get()->result_array();
		if (!empty($get_language)) {
			foreach ($get_language as $key => $value) {
				$dt_language[] = [
					'id_language' => $value['id'],
					'id_employee' => $value['id_employee'],
					'id_language_certificate' => $value['id_language_certificate'],
					'language' => $value['language'],
					'language_test_institute' => $value['language_test_institute'],
					'test_score' => $value['test_score'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}

			$this->db->insert_batch('temporary_employee_language', $dt_language);
		}

		$this->db->select($this->selecArr('employee_professional_membership'))
			->from('employee_professional_membership epm')
			->join("ref_file_uploaded as membership_doc", "membership_doc.id = epm.id_membership_document", "left")
			->where('epm.id_employee', decrypt_url($params['id']));
		$get_membership = $this->db->get()->result_array();
		if (!empty($get_membership)) {
			foreach ($get_membership as $value) {
				$dt_membership[] = [
					'id_membership' => $value['id'],
					'id_employee' => $value['id_employee'],
					'id_membership_document' => $value['id_membership_document'],
					'professional_membership_association' => $value['professional_membership_association'],
					'roles_to_association' => $value['roles_to_association'],
					'membership_time' => $value['membership_time'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}

			$this->db->insert_batch('temporary_employee_professional_membership', $dt_membership);
		}

		$this->db->select($this->selecArr('employee_work_experience'))
			->from('employee_work_experience ewe')
			->join("ref_file_uploaded as certificate", "certificate.id = ewe.id_certificate_employment", "left")
			->join("ref_file_uploaded as salary_slip", "salary_slip.id = ewe.id_salary_slip", "left")
			->where('ewe.id_employee', decrypt_url($params['id']));
		$get_experiences = $this->db->get()->result_array();
		if (!empty($get_experiences)) {
			foreach ($get_experiences as $value) {
				$dt_experiences[] = [
					'id_experience' => $value['id'],
					'id_employee' => $value['id_employee'],
					'id_certificate_employment' => $value['id_certificate_employment'],
					'id_salary_slip' => $value['id_salary_slip'],
					'work_type' => $value['work_type'],
					'total_time_experience' => $value['total_time_experience'],
					'last_position_title' => $value['last_position_title'],
					'last_roles' => $value['last_roles'],
					'employment_status' => $value['employment_status'],
					'last_company' => $value['last_company'],
					'last_working_tenure' => $value['last_working_tenure'],
					'reason_for_leaving' => $value['reason_for_leaving'],
					'last_salary' => $value['last_salary'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}
			$this->db->insert_batch('temporary_employee_work_experience', $dt_experiences);
		}

		$get_medical = $this->db->select($this->selecArr('employee_medical_record'))->where('id_employee', decrypt_url($params['id']))->get('employee_medical_record')->result_array();
		if (!empty($get_medical)) {
			foreach ($get_medical as $value) {
				$dt_medical = [
					'id_medical' => $value['id'],
					'id_employee' => $value['id_employee'],
					'serious_disease' => $value['serious_disease'],
					'desc_disease' => $value['desc_disease'],
					'serious_accident' => $value['serious_accident'],
					'desc_accident' => $value['desc_accident'],
					'color_blindness' => $value['color_blindness'],
					'other_medical_issue' => $value['other_medical_issue'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}
			$this->db->insert('temporary_employee_medical_record', $dt_medical);
		}
		$get_references = $this->db->select($this->selecArr('employee_references'))->where('id_employee', decrypt_url($params['id']))->get('employee_references')->result_array();
		if (!empty($get_references)) {
			foreach ($get_references as $value) {
				$dt_references[] = [
					'id_references' => $value['id'],
					'id_employee' => $value['id_employee'],
					'name' => $value['name'],
					'job_title' => $value['job_title'],
					'company' => $value['company'],
					'affilition' => $value['affilition'],
					'contact_number' => $value['contact_number'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}

			$this->db->insert_batch('temporary_employee_references', $dt_references);
		}

		$this->db->select($this->selecArr('employee_award'))
			->from('employee_award')
			->join("ref_file_uploaded as award_doc", "award_doc.id = employee_award.id_award_document", "left")
			->where('employee_award.id_employee', decrypt_url($params['id']));
		$get_award = $this->db->get()->result_array();
		if (!empty($get_award)) {
			foreach ($get_award as $value) {
				$dt_award[] = [
					'id_award' => $value['id'],
					'id_employee' => $value['id_employee'],
					'id_award_document' => $value['id_award_document'],
					'type_award' => $value['type_award'],
					'award_name' => $value['award_name'],
					'year_of_award' => $value['year_of_award'],
					'roles_to_the_award' => $value['roles_to_the_award'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s'),
				];
			}

			$this->db->insert_batch('temporary_employee_award', $dt_award);
		}

		return [
			'message' => 'save temporary success',
			'status' => 200,
		];
	}

	public function getDetail($params)
	{
		$file_tab_profile = [];

		$this->db->select($this->selecArr('employee_personal_data'))
			->from('employee_personal_data')
			->join('users', 'users.id = employee_personal_data.id_users', 'left')
			->join("ref_file_uploaded as tbl_profile", "tbl_profile.id = employee_personal_data.id_photo_profile", "left")
			->join("ref_file_uploaded as tbl_file_citizen", "tbl_file_citizen.id = employee_personal_data.id_photo_citizen_number", "left")
			->where('employee_personal_data.id', decrypt_url($params['id']));
		$get_personal_data = $this->db->get()->first_row();


		$this->db->select($this->selecArr('employee_educational_background'))
			->from('employee_educational_background')
			->join("ref_file_uploaded as tbl_certificate", "tbl_certificate.id = employee_educational_background.id_certificate", "left")
			->where('employee_educational_background.id_employee', decrypt_url($params['id']));
		$get_education = $this->db->get()->result_array();

		$this->db->select($this->selecArr('employee_family'))
			->from('employee_family')
			->join("ref_file_uploaded as tbl_family_card", "tbl_family_card.id = employee_family.id_family_card", "left")
			->where('employee_family.id_employee', decrypt_url($params['id']));
		$get_family = $this->db->get()->result_array();

		$this->db->select($this->selecArr('employee_language'))
			->from('employee_language')
			->join("ref_file_uploaded as certificate_language", "certificate_language.id = employee_language.id_language_certificate", "left")
			->where('employee_language.id_employee', decrypt_url($params['id']));
		$get_language = $this->db->get()->result_array();


		$this->db->select($this->selecArr('employee_professional_membership'))
			->from('employee_professional_membership epm')
			->join("ref_file_uploaded as membership_doc", "membership_doc.id = epm.id_membership_document", "left")
			->where('epm.id_employee', decrypt_url($params['id']));
		$get_membership = $this->db->get()->result_array();

		$this->db->select($this->selecArr('employee_work_experience'))
			->from('employee_work_experience ewe')
			->join("ref_file_uploaded as certificate", "certificate.id = ewe.id_certificate_employment", "left")
			->join("ref_file_uploaded as salary_slip", "salary_slip.id = ewe.id_salary_slip", "left")
			->where('ewe.id_employee', decrypt_url($params['id']));
		$get_experiences = $this->db->get()->result_array();

		$get_medical = $this->db->select($this->selecArr('employee_medical_record'))->where('id_employee', decrypt_url($params['id']))->get('employee_medical_record')->row();

		$get_references = $this->db->select($this->selecArr('employee_references'))->where('id_employee', decrypt_url($params['id']))->get('employee_references')->result_array();

		$this->db->select($this->selecArr('employee_award'))
			->from('employee_award')
			->join("ref_file_uploaded as award_doc", "award_doc.id = employee_award.id_award_document", "left")
			->where('employee_award.id_employee', decrypt_url($params['id']));
		$get_award = $this->db->get()->result_array();

		if ($params['menu'] == 'detail') {
			$get_personal_data->full_name = strtoupper($get_personal_data->full_name);
			$get_personal_data->date_of_place = ucfirst($get_personal_data->date_of_place);
			$get_personal_data->date_of_birth = date('d-M-Y', strtotime($get_personal_data->date_of_birth));
			$get_personal_data->photo_profile = !empty($get_personal_data->photo_profile) ? $get_personal_data->photo_profile : base_url('assets/img/avatar/avatar-5.png');
			$social_media_accounts = !empty($get_personal_data->social_media_app) ? settingSocialMedia($get_personal_data->social_media_app) : [];


			if (!empty($get_personal_data->sex)) {
				$get_personal_data->sex = strtolower($get_personal_data->sex) == 'male' ? '<i class="text-25 fas fa-mars"></i>' : '<i class="text-25 fas fa-venus"></i>';
			} else {
				$get_personal_data->sex = '<i class="text-25 fas fa-venus-mars"></i>';
			}

			if (!empty($get_family)) {
				foreach ($get_family as $key => $value) {
					$get_family[$key] = [
						'id' => $value['id'],
						'family_type' => ucfirst($value['family_type']),
						'citizen_number' => $value['citizen_number'],
						'full_name' => ucfirst($value['full_name']),
						'place_of_birth' => ucfirst($value['place_of_birth']),
						'date_of_birth' => date('d-M-Y', strtotime($value['date_of_birth'])),
						'latest_education' => ucfirst($value['latest_education']),
						'gender' => ucfirst($value['gender']),
						'occupation_title' => ucfirst($value['occupation_title']),
						'company' => ucfirst($value['company']),
						'company_address' => $value['company_address'],
						'contact_number' => $value['contact_number'],
						'height' => $value['height'],
						'weight' => $value['weight'],
						'blood_type' => ucfirst($value['blood_type']),
						'religion' => ucfirst($value['religion']),
						'current_address' => ucfirst($value['current_address']),
						'current_city' => ucfirst($value['current_city']),
						'family_card' => $value['family_card'],
						'family_card_type' => $value['family_card_type'],
					];
				}
			}

			if (!empty($get_language)) {
				foreach ($get_language as $key => $value) {
					$get_language[$key]['icon_language'] = !empty(country($value['language'])->code) ? 'flag-icon-' . country($value['language'])->code : 'flag-icon-gb';
				}
			}
		}

		return [
			'personal' => $get_personal_data,
			'file_profile' => $file_tab_profile,
			'education' => !empty($get_education) ? $get_education : null,
			'family' => !empty($get_family) ? $get_family : null,
			'language' => !empty($get_language) ? $get_language : null,
			'membership' => !empty($get_membership) ? $get_membership : null,
			'experiences' => !empty($get_experiences) ? $get_experiences : null,
			'medical' => !empty($get_medical) ? $get_medical : null,
			'references' => !empty($get_references) ? $get_references : null,
			'social_media_accounts' => !empty($social_media_accounts) ? $social_media_accounts : null,
			'award' => !empty($get_award) ? $get_award : null,
			'status' => 200,
			'message' => 'successfuly',
		];
	}
}
