<style>
    .table tr td {
        vertical-align: middle !important;
    }
</style>

<div class="main-content">
    <section class="section">
    	<form class="access-form" action="<?= $formURL ?>" method="post" enctype="multipart/form-data">
	        <div class="row">
	        	<div class="col-12 col-lg-6">
	        		<div class="card">
						<div class="card-header">
							<h5>Main Form</h5>
						</div>
						<div class="card-body">
							<div class="form-group row">
								<label for="group_name">Role Name</label>
								<input type="text" class="form-control" id="role_name" name="role_name" value="<?= !empty($roles->role_name) ? $roles->role_name : '' ?>" placeholder="Enter Role Name" required="">
							</div>
							<div class="form-group row">
								<label for="group_description">Role Description</label>
								<textarea class="form-control" id="role_description" name="role_description" placeholder="Write your messages"><?= !empty($roles->role_description) ? $roles->role_description : '' ?></textarea>
							</div>
							<div class="form-group row">
								<label for="role_status">Role Status</label>
								<select class="form-control" id="role_status" name="role_status">
									<option value="0" <?= !empty($roles->role_status) && $roles->role_status == 0 ? 'selected' : '' ?>>Non-Aktif</option>
									<option value="1" <?= !empty($roles->role_status) && $roles->role_status == 1 ? 'selected' : '' ?>>Aktif</option>
								</select>
							</div>
						</div>
					</div>
	        	</div>

	        	<div class="col-12 col-lg-6">
	        		<div class="card">
						<div class="card-header">
							<h5>Access Menu</h5>
						</div>
						<div class="card-body">
							<div id="accordion1">
								<?php foreach($accessListNew as $moduleName => $moduleClass) : ?>
					            <div class="card mb-2">
					                <div class="card-header p-0 gradient-dusk">
					                    <button type="button" class="btn btn-link text-white shadow-none" data-toggle="collapse" data-target="#collapse-<?= $moduleName ?>" aria-expanded="true" aria-controls="collapse-<?= $moduleName ?>">
					                      <?= str_replace("_", " ", $moduleName) ?>
					                    </button>
					                </div>

					                <div id="collapse-<?= $moduleName ?>" class="collapse" data-parent="#accordion1">
					                  <div class="card-body">
											<?php foreach($moduleClass as $class => $valClass) :?>
												<?php if ($class != "0") { ?>
												<div class="row">
													<div class="col-6">
														<p style="font-size: 12px;"><?= str_replace("_", " ", strtoupper(str_replace("access", " ", $class))) ?>
													</div>
													<div class="col-1 text-center">
														<p> : </p>
													</div>
													<div class="col-5">
														<div class="icheck-material-danger icheck-inline">
															<input type="radio" id="radio-<?= $moduleName ?>-<?= $class ?>-off" name="access[<?= $moduleName ?>][<?= $class?>]" value="off" checked=""/>
															<label for="radio-<?= $moduleName ?>-<?= $class ?>-off">OFF</label>
														</div>
														<div class="icheck-material-success icheck-inline">
															<input type="radio" id="radio-<?= $moduleName ?>-<?= $class ?>-on" name="access[<?= $moduleName ?>][<?= $class?>]" value="on" <?= @$access->{$moduleName}->{$class} == 'on' ? 'checked=""' : ''; ?> />
															<label for="radio-<?= $moduleName ?>-<?= $class ?>-on">ON</label>
														</div>
													</div>
												</div>
												<?php } ?>
											<?php endforeach; ?>
					                  </div>
					                </div>
					            </div>
					        	<?php endforeach; ?>
				            </div>
						</div>
					</div>
	        	</div>
	    	</div>
	    	<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body clearfix">
							<button class="btn btn-primary float-right" type="submit">Submit</button>
							<a href="<?= site_url('user_role') ?>" class="btn btn-secondary float-right mr-1">Back</a>
						</div>
					</div>
				</div>
			</div>
	    </form>
    </section>
</div>