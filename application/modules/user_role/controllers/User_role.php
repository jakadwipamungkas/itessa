<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User_role extends Tessa_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('user_role_model', 'role_model');
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'iTessa';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/js/app/user_role.js?'.rand()
		);	

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
		);

		$this->template->load($this->data, null, 'index');

	}

	public function get_role_get()
	{
		$formdata = $this->input->get();
		$keyword = $formdata['keyword'];
		$column = !empty($formdata['column']) ? true : false;
		
		$pagination = [
			'page'  =>  !empty($formdata['page']) ? $formdata['page'] : null, 
			'limit' =>  !empty($formdata['limit']) ? $formdata['limit'] : null
		];

		$data = $this->role_model->getAll($keyword, $pagination, $column);

        // print_r("<pre>");
		// print_r($data);
		// exit();

        $this->set_response($data);
	}

	public function add_role_get()
	{
		$this->data['formURL'] = site_url('user_role/add_role');
		$this->data['title']   = 'Register Role';
		$this->data['accessListNew'] = json_decode('
			{
				"requitment":
					{
						"dashboard":"off",
						"accessadd_dashboard":"off",
						"accessedit_dashboard":"off",
						"accessdelete_dashboard":"off"
					},
				"employee":
					{
						"employee":"off",
						"accessadd_employee":"off",
						"accessedit_employee":"off",
						"accessdelete_employee":"off"
					},
				"human_capital":
					{
						"human_capital":"off",
						"accessadd_human_capital":"off",
						"accessedit_human_capital":"off",
						"accessdelete_human_capital":"off"
					},
				"faq":
					{
						"faq":"off",
						"accessadd_faq":"off",
						"accessedit_faq":"off",
						"accessdelete_faq":"off"
					},
				"user_management":
					{
						"user_management":"off",
						"accessadd_user_management":"off",
						"accessedit_user_management":"off",
						"accessdelete_user_management":"off"
					}
				}
			');

		$this->template->load($this->data, null, 'role_form');	
	}

	public function add_role_post()
	{
		$this->form_validation->set_rules('role_name', 'Role Name', 'required|min_length[3]');
        $this->form_validation->set_rules('role_description', 'Role Description', 'trim|required|min_length[5]');    
      
        if($this->form_validation->run() == FALSE):
            $this->session->set_flashdata('alert-warning', validation_errors());
            redirect(base_url().'user_role/add_role', 'refresh');
        else:
            $data = array(
                "role_name"         => $this->input->post('role_name', TRUE),
                "role_desc"  => $this->input->post('role_description', TRUE),
                "role_access"       => json_encode($this->input->post('access', TRUE), JSON_NUMERIC_CHECK),
                "role_status"       => !empty($this->input->post('role_status')) ? $this->input->post('role_status', TRUE) : 0,
                // "created_at"        => $this->session->userdata('authDPIM')['user_id'],
                "created_at"      => date('Y-m-d H:i:s'),
            );
          
            $Insert = $this->role_model->add($data);

            if ($Insert) {
                $this->session->set_flashdata('alert-success', "Success Add New User Role");
                redirect(base_url().'user_role', 'refresh');
            }else{
                $this->session->set_flashdata('alert-error', "Failed to add new user Role");
                redirect(base_url().'user_role', 'refresh');
            }
        endif;
	}

	public function edit_role_get($id)
	{
		
		// Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title']   = 'Register Role';
		$this->data['formURL'] = site_url('user_role/edit_role/' . $id);
		// $this->data['accessList'] = $this->controllerlist->run('class')->getData();
		
		//role access default
		$default_access = json_decode('
			{
				"requitment":
					{
						"dashboard":"off",
						"accessadd_dashboard":"off",
						"accessedit_dashboard":"off",
						"accessdelete_dashboard":"off"
					},
				"employee":
					{
						"employee":"off",
						"accessadd_employee":"off",
						"accessedit_employee":"off",
						"accessdelete_employee":"off"
					},
				"human_capital":
					{
						"human_capital":"off",
						"accessadd_human_capital":"off",
						"accessedit_human_capital":"off",
						"accessdelete_human_capital":"off"
					},
				"faq":
					{
						"faq":"off",
						"accessadd_faq":"off",
						"accessedit_faq":"off",
						"accessdelete_faq":"off"
					},
				"user_management":
					{
						"user_management":"off",
						"accessadd_user_management":"off",
						"accessedit_user_management":"off",
						"accessdelete_user_management":"off"
					}
				}
			');
		$role_access = $this->db->select("*")->from("users_role")->where("id", $id)->get()->result_array()[0]["role_access"];		
		$this->data['accessListNew'] = !empty($role_access) ? json_decode($role_access) : $default_access;
		$roles = $this->db->select('*')->from('users_role')->where('id = ', $id)->get()->first_row();
		$this->data['roles']   = $roles;
		$this->data['access']  = !empty($this->data['roles']->role_access) ? json_decode($this->data['roles']->role_access) : array();
        // $this->data['aliasMenu'] = $this->lib_menu();
        
        // echo "<pre>";print_r($this->data);die;
		$this->template->load($this->data, null, 'role_form', 'user_role');	
	}

	public function edit_role_post($id)
	{
		$this->form_validation->set_rules('role_name', 'Role Name', 'required|min_length[3]');
        $this->form_validation->set_rules('role_description', 'Role Description', 'trim|required|min_length[5]');    

		if($this->form_validation->run() == FALSE):
            $this->session->set_flashdata('alert-warning', validation_errors());
            redirect(base_url().'user_role/edit_role/' . $id, 'refresh');
        else:
			$data = array(
	            "role_name"        => $this->input->post('role_name', TRUE),
	            "role_desc" 	   => $this->input->post('role_description', TRUE),
	            "role_access"      => json_encode($this->input->post('access', TRUE), JSON_NUMERIC_CHECK),
	            "role_status"      => !empty($this->input->post('role_status')) ? $this->input->post('role_status', TRUE) : 0,
	            // "updated_at"       => $this->session->userdata('authTessa')['id'],
	            "updated_at"     => date('Y-m-d H:i:s'),
	        );			
	      	
	        $Insert = $this->db->where("id = ", $id)->update('users_role', $data);

	        if ($Insert) {
	            // update current logged user access privilege if any
	            if ($role_id == $this->session->userdata('user_role')) :
	                $this->session->set_userdata( array(
	                    'user_access' => json_decode($data['role_access'], TRUE)
	                ));
	            endif;

	            // redirect to main role view with message
	            $this->session->set_flashdata('alert-success', "Success Add New User Role");
	            redirect(base_url().'user_role', 'refresh');
	        }else{
	            $this->session->set_flashdata('alert-error', "Failed to Add New User Role");
	            redirect(base_url().'user_role/edit_role/' . $id, 'refresh');
	        }
        endif;
	}
}
