<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Candidate extends Tessa_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('candidate_model');
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'iTessa';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/js/app/candidate.js?'.rand()
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
		);

		$this->template->load($this->data, null, 'index');

	}

	public function get_candidate_get()
	{
		$formdata = $this->input->get();
		$keyword = $formdata['keyword'];
		$column = !empty($formdata['column']) ? true : false;
		
		$pagination = [
			'page'  =>  !empty($formdata['page']) ? $formdata['page'] : null, 
			'limit' =>  !empty($formdata['limit']) ? $formdata['limit'] : null
		];

		$data = $this->candidate_model->getAll($keyword, $pagination, $column);

        // print_r("<pre>");
		// print_r($data);
		// exit();

        $this->set_response($data);
	}
}
