<style>
    .table tr td {
        vertical-align: middle !important;
    }
</style>
<div class="main-content" ng-controller="candidate" id="candidate">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats p-3">
                        <div class="row mb-5 mt-2">
                            <div class="col-lg-6 col-md-6 col-sm-6 mb-2">
                                <h6><i class="fas fa-user-tag"></i> Candidate List</h6>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 mb-2">
                                <button class="btn btn-success btn-sm" style="float: right;"><i class="fas fa-file-excel"></i> Export</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div ng-if="pending">
                                    <center><img src="<?= base_url() ?>assets/img/loadertsel.gif" alt="img-fluid"> Loading...</center>
                                </div>
                                <div class="table-responsive" ng-if="!pending">
                                    <table class="table table-striped table-md" style="font-size: 12px;">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Unit</th>
                                                <th class="text-center">Position</th>
                                                <th class="text-center">Track Candidate</th>
                                                <th class="text-center">Apply Date</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <th>
                                                    <input type="text" id="filter_name" class="form-control">
                                                </th>
                                                <th>
                                                    <input type="text" id="filter_email" class="form-control">
                                                </th>
                                                <th>
                                                    <input type="text" id="filter_position" class="form-control">
                                                </th>
                                                <th>
                                                    <select id="filter_track" class="form-control" name="filter_track">
                                                        <option value="" selected>- Choose -</option>
                                                        <option value="email_sent">Email Sent</option>
                                                        <option value="email_opened">Email opened</option>
                                                        <option value="form_opened">Form opened</option>
                                                        <option value="form_updated">Form updated</option>
                                                    </select>
                                                </th>
                                                <th>
                                                    <input type="date" id="filter_applied" class="form-control">
                                                </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr dir-paginate="(key, value) in itemData|itemsPerPage:itemsPerPage" total-items="totalData" current-page="currentPage" pagination-id="paginateID">
                                                <td style="font-size: 10px;" class="text-center" ng-bind="key+no"></td>
                                                <td ng-bind="value.fullname"></td>
                                                <td ng-bind="value.ref_position"></td>
                                                <td ng-bind="value.ref_position"></td>
                                                <td>
                                                    <span class="badge badge-success" style="font-size: 10px;">Email sent</span>
                                                </td>
                                                <td ng-bind="value.created_at"></td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm btn-save-family"> <i class="fas fa-info-circle"></i></button>
                                                    <button type="button" class="btn btn-warning btn-sm btn-save-family" ng-click="invite(value)"><i class="fas fa-paper-plane"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <dir-pagination-controls 
                                    max-size="8" 
                                    template-url="<?= base_url('assets/js/angular/') ?>/dirPagination.tpl.html" 
                                    direction-links="true" 
                                    pagination-id="paginateID"
                                    boundary-links="true" 
                                    on-page-change="getCandidate(newPageNumber)">
                                </dir-pagination-controls>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal fade" id="mdlInvite" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlInviteLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mdlInviteLabel">Send Invitation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="email_candidate">Email address</label>
                                    <input type="email" class="form-control" id="email_candidate" ng-model="candidate.email" aria-describedby="emailHelp" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="start_date">Date</label>
                                    <input type="datetime-local" class="form-control" id="start_date" ng-model="candidate.date" aria-describedby="emailHelp">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control" id="location" ng-model="candidate.date" aria-describedby="emailHelp">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </div>
</div>