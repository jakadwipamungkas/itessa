<?php
class Candidate_model extends CI_Model {

	public function getAll($keyword, $pagination, $column)
    {
        if (! empty((array) json_decode($keyword))) {

            $w = 0;
            $where = "WHERE ";

            foreach (json_decode($keyword) as $key => $value) {

                $key = $key;
                if ($w == 0) {

                    if (is_array($value)) {
                        $where .= $key." IN ('".implode("','", $value)."') ";
                    }else
                        $where .= $key." LIKE '%".$value."%' ";

                }else{
                    $where .= " AND ".$key." LIKE '%".$value."%' ";
                }

                $w++;
            }
        }
        else
        {
            $where = '';
        }

        if ($pagination['page'] != null) {
            $skip = $pagination['page'] - 1;
            $skip = ($skip == 0) ? 0 : $skip * $pagination['limit'];
            $limit = " LIMIT ".$pagination['limit']." OFFSET ".$skip;
        }else
            $limit = "";

        $sql    = $this->db->query("SELECT * FROM candidate_career_telin ORDER BY created_at DESC ".$where." ".$limit)->result();
        
        // print_r($this->db->last_query());
        // exit();

        $count = $this->db->query("SELECT count(*) total FROM candidate_career_telin ".$where." ")->result()[0]->total;
        
        $data = ['page' => $pagination['page'], 'total' => $count, 'data' => $sql];

        return $data;
    }
}