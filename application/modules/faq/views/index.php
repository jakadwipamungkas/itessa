<style>
	.table tr td {
		vertical-align: middle !important;
	}

	.loader-img {
		width: 25px !important;
	}
</style>
<div class="main-content" ng-controller="faq" id="faq">
	<section class="section">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-lg-6 col-md-6 col-sm-6 ">
								<h6><i class="far fa-question-circle"></i> Faq List</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<form class="form-inline float-right">
									<div class="mb-2 mr-2">
										<input type="search" class="form-control" placeholder="Keyword..." ng-model="keyword" ng-model-options="{debounce: 500}" ng-change="search()">
									</div>
									<div class="mb-2 mr-2">
										<a href="<?= base_url('faq/add') ?>" title="Add Faq" class="btn btn-primary-custom">
											<i class="fas fa-plus"></i>
										</a>
									</div>
								</form>
								<div class="table-responsive">
									<table class="table table-striped table-md" style="font-size: 12px;">
										<thead>
											<tr>
												<th>#</th>
												<th class="text-center">Title</th>
												<th class="text-center">Public Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-show="message != null">
												<td colspan="3" class="text-center" ng-bind="message"></td>
											</tr>
											<tr>
												<td colspan="3" ng-show="loading">
													<img class="loader-img" src="<?= base_url('assets/img/loadertsel.gif') ?>" alt="loader">
													Loading...
												</td>
											</tr>
											<tr ng-hide="loading" dir-paginate="(key, value) in data|itemsPerPage:itemsPerPage" total-items="total_count" current-page="curPage" pagination-id="paginateID">
												<td ng-bind="key+pageno"></td>
												<td ng-bind="value.title"></td>
												<td class="text-center">
													<label class="switch switch-success">
														<input type="checkbox" ng-model="value.public_status" ng-change="changeStatus(value)" />
														<span class="slider"></span>
													</label>
												</td>
												<td class="text-center">
													<a href="<?= base_url('faq/edit'); ?>?id={{value.id}}" class="btn btn-info btn-sm" title="Edit title: {{value.title}}">
														<i class="fas fa-edit"></i>
													</a>
													<button type="button" class="btn btn-danger btn-sm" title="Delete title: {{value.title}}" ng-click="delete(value)">
														<i class="fas fa-trash"></i>
													</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>

								<dir-pagination-controls max-size="8" template-url="<?= base_url('assets/js/angular/') ?>/dirPagination.tpl.html" direction-links="true" pagination-id="paginateID" boundary-links="true" on-page-change="getFaq(newPageNumber)">
								</dir-pagination-controls>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="mdlInvite" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="mdlInviteLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="mdlInviteLabel">Send Invitation</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="form-group">
									<label for="email_candidate">Email address</label>
									<input type="email" class="form-control" id="email_candidate" ng-model="candidate.email" aria-describedby="emailHelp" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="form-group">
									<label for="start_date">Date</label>
									<input type="datetime-local" class="form-control" id="start_date" ng-model="candidate.date" aria-describedby="emailHelp">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="form-group">
									<label for="location">Location</label>
									<input type="text" class="form-control" id="location" ng-model="candidate.date" aria-describedby="emailHelp">
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Send</button>
				</div>
			</div>
		</div>
	</div>
</div>