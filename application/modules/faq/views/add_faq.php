<style>
	.loader-img {
		width: 25px !important;
	}

	.section-url {
		color: #6c757d;
		font-weight: bold;
	}

	.section-url:hover {
		color: #535b61;
		font-weight: bold;
	}

	.form-group label {
		font-size: 14px !important;
	}
</style>
<div class="main-content" ng-controller="form_faq" id="form_faq">
	<section class="section">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-lg-6 col-md-6 col-sm-6 ">
								<h6><i class="far fa-question-circle"></i> <a class="url" href="<?= base_url('faq') ?>">Faq</a>
									| Add
								</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-status p-4">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<form ng-submit="prosesFaq()">
									<div class="form-group">
										<label for="title">Title</label>
										<input type="text" class="form-control" id="title" name="title" ng-model="title" placeholder="Input Title">
									</div>
									<div class="form-group">
										<label for="description">Description</label>
										<textarea class="form-control" id="description" rows="8" name="description" ng-model="description" ui-tinymce="tinymceOptions"></textarea>
									</div>
									<div class="form-group">
										<button class="btn btn-danger btn-lg btn-block" type="submit">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>