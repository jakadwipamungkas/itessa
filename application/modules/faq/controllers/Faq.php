<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends Tessa_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('faq_model');
	}

	// list faq

	public function index_get()
	{
		$this->data['title'] = 'Faq ~ iTessa';
		$this->data['version'] = $this->uri->segment(1);
		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];

		$this->data['js'] = array(
			'assets/js/app/faq.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
		);

		$this->template->load($this->data, null, 'index');
	}

	public function getDataFaq_get()
	{
		$return = $this->faq_model->getDataFaq($this->get());

		$this->response($return, 200);
	}

	// list faq end

	public function add_get()
	{
		$this->data['title'] = 'Add Faq ~ iTessa';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			'assets/js/app/form_faq.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js'
		);

		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];


		$this->template->load($this->data, null, 'add_faq');
	}

	public function prosesFaq_post()
	{
		$params = $this->post();
		$params['updated_by'] = users_sessions('id');
		$params['updated_at'] = date('Y-m-d H:i:s');
		if (empty($params['id'])) {
			$params['created_by'] = users_sessions('id');
			$params['created_at'] = date('Y-m-d H:i:s');
		}
		$return = $this->faq_model->prosesFaq($params);

		$this->response($return, $return['status']);
	}

	public function edit_get()
	{
		$this->data['title'] = 'Edit Faq ~ iTessa';
		$this->data['version'] = $this->uri->segment(1);
		$this->data['id'] = $this->get('id');

		$this->data['js'] = array(
			'assets/js/app/form_faq.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js'
		);

		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];


		$this->template->load($this->data, null, 'edit_faq');
	}

	public function getFaqById_get()
	{
		$return = $this->faq_model->getFaqById($this->get('id'));
		$this->response($return, $return['status']);
	}

	public function deleteFaq_post()
	{
		$return = $this->faq_model->deleteFaq($this->post('id'));
		$this->response($return, $return['status']);
	}
}
