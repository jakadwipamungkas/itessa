<?php
class Faq_model extends CI_Model
{
    public function getDataFaq($params)
    {
        $start = ($params['offset'] - 1) * $params['limit'];

        $this->db->select('id,title,public_status');
        if (!empty($params['keyword'])) {
            $this->db->like('title', $params['keyword']);
        }
        $this->db->limit($params['limit'], $start)->order_by('title', 'ASC');
        $get_data = $this->db->get('faq')->result_array();

        $this->db->select('id');
        if (!empty($params['keyword'])) {
            $this->db->like('title', $params['keyword']);
        }
        $this->db->order_by('title', 'ASC');
        $get_count = $this->db->get('faq')->num_rows();

        if (!empty($get_data)) {
            foreach ($get_data as $key => $value) {
                $get_data[$key] = [
                    'id' => $value['id'],
                    'title' => $value['title'],
                    'public_status' => $value['public_status'] == '1' ? true : false,
                ];
            }
        }


        return [
            'count' => $get_count,
            'data' => !empty($get_data) ? $get_data : [],
            'message' => !empty($get_data) ? null : 'Data Empty!',
        ];
    }

    public function prosesFaq($params)
    {
        if (!empty($params['id'])) {
            $id = $params['id'];
            unset($params['id']);

            if ($this->db->where('id', $id)->update('faq', $params)) {
                return [
                    'message' => 'Update Faq Success',
                    'status' => 200,
                ];
            }

            return [
                'message' => 'Update Faq Failed with error code 400, Please Refresh page or contact admin!',
                'status' => 400,
            ];
        }

        if ($this->db->insert('faq', $params)) {
            return [
                'message' => 'Add Faq Success',
                'status' => 200,
            ];
        }

        return [
            'message' => 'Add Faq Failed with error code 400, Please Refresh page or contact admin!',
            'status' => 400,
        ];
    }

    public function getFaqById($id)
    {
        $this->db->select('title,description')
            ->where('id', $id);
        $data =  $this->db->get('faq')->row();

        return [
            'status' => empty($data) ? 500 : 200,
            'message' => empty($data) ? 'Data not found!' : null,
            'data' => !empty($data) ? $data : [],
        ];
    }


    public function deleteFaq($id)
    {
        if ($this->db->delete('faq', ['id' => $id])) {
            return [
                'message' => 'Delete success',
                'status' => 200,
            ];
        }

        return [
            'message' => 'Delete failed, please refresh page!',
            'status' => 400
        ];
    }
}
