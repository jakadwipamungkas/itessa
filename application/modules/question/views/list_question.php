<style>
	.table tr td {
		vertical-align: middle !important;
	}

	.loader-img {
		width: 25px !important;
	}

</style>
<div class="main-content" ng-controller="question" id="question">
	<section class="section">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-lg-6 col-md-6 col-sm-6 ">
								<h6><i class="far fa-question-circle"></i> Question List</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<form class="form-inline float-right">
									<div class="mb-2 mr-2">
										<input type="search" class="form-control" placeholder="Keyword..."
											ng-model="keyword" ng-model-options="{debounce: 500}" ng-change="search()">
									</div>
								</form>
								<div class="table-responsive">
									<table class="table table-striped table-md" style="font-size: 12px;">
										<thead>
											<tr>
												<th>#</th>
												<th class="text-center">Question</th>
												<th class="text-center">status</th>
												<th class="text-center">Question Name</th>
												<th class="text-center">Answer Name</th>
												<th class="text-center">Question Time</th>
												<th class="text-center">Public Status</th>
												<th class="text-center">Tools</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-show="message != null">
												<td colspan="8" class="text-center" ng-bind="message"></td>
											</tr>
											<tr>
												<td colspan="8" ng-show="loading">
													<img class="loader-img"
														src="<?= base_url('assets/img/loadertsel.gif') ?>" alt="loader">
													Loading...
												</td>
											</tr>
											<tr ng-hide="loading"
												dir-paginate="(key, value) in data|itemsPerPage:itemsPerPage"
												total-items="total_count" current-page="curPage"
												pagination-id="paginateID">
												<td class="text-center" ng-bind="key+pageno"></td>
												<td class="text-left" ng-bind="value.question"></td>
												<td class="text-center" ng-bind-html="value.status"></td>
												<td class="text-center" ng-bind="value.name_question"></td>
												<td class="text-center" ng-bind="value.name_answerer"></td>
												<td class="text-center" ng-bind="value.question_time"></td>
												<td class="text-center">
													<label class="switch switch-success">
														<input type="checkbox" ng-model="value.public_status"
															ng-change="changeStatus(value)" />
														<span class="slider"></span>
													</label>
												</td>
												<td class="text-center">
													<a href="<?= base_url('question/answer'); ?>?id={{value.id}}"
														class="btn btn-info btn-sm"
														title="Answer Question: {{value.question}}">
														<i class="fas fa-edit"></i>
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>

								<dir-pagination-controls max-size="8"
									template-url="<?= base_url('assets/js/angular/') ?>/dirPagination.tpl.html"
									direction-links="true" pagination-id="paginateID" boundary-links="true"
									on-page-change="getQuestion(newPageNumber)">
								</dir-pagination-controls>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
