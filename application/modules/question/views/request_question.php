<style>
	.loader-img {
		width: 25px !important;
	}

	.img-custom {
		height: 25%;
		width: 25%;
	}
</style>
<div class="main-content" ng-controller="question" id="question">
	<section class="section">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-lg-6 col-md-6 col-sm-6 ">
								<h6><i class="far fa-question-circle"></i> <a class="url" href="<?= base_url('faq') ?>">Question</a> |
									Request
								</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row">
							<div class="col-12 col-md-12 col-lg-12">
								<form ng-submit="addQuestion()">
									<div class="form-group">
										<label for="question">Question</label>
										<input type="text" class="form-control" id="question" name="question" ng-model="question" placeholder="Input Question">
									</div>
									<div class="text-right">
										<button class="btn btn-danger btn-lg" type="submit">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>