<style>
	.loader-img {
		width: 25px !important;
	}

	.form-group label {
		font-size: 14px !important;
	}
</style>
<div class="main-content" ng-controller="question" id="question" data-idQuestion="<?= $id ?>">
	<section class="section">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-lg-6 col-md-6 col-sm-6 ">
								<h6><i class="far fa-question-circle"></i> <a class="url" href="<?= base_url('question/listQuestion') ?>">Question</a>
									| Answer
								</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-status p-3">
						<form ng-submit="saveAnswer()">
							<div class="row">
								<div class="col-12 col-md-12 col-lg-3">
									<div class="form-group">
										<label for="question_by">Question By</label>
										<input type="text" disabled class="form-control" id="question_by" name="question_by" ng-model="question_by">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-6">
									<div class="form-group">
										<label for="question">Question</label>
										<input type="text" disabled class="form-control" id="question" name="question" ng-model="question">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-3">
									<div class="form-group">
										<label for="question_time">Question Time</label>
										<input type="text" disabled class="form-control" id="question_time" name="question_time" ng-model="question_time">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-4">
									<div class="form-group">
										<label for="answer_by">Answer By</label>
										<input type="text" disabled class="form-control" id="answer_by" name="answer_by" ng-model="answer_by">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-4">
									<div class="form-group">
										<label for="answer_time">Answer Time</label>
										<input type="text" disabled class="form-control" id="answer_time" name="answer_time" ng-model="answer_time">
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-4">
									<label for="public_status">Status Public</label>
									<div class="form-group">
										<label class="switch switch-success">
											<input type="checkbox" ng-model="public_status" />
											<span class="slider"></span>
										</label>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-12">
									<div class="form-group">
										<label for="answer">Answer</label>
										<textarea class="form-control" id="answer" rows="8" name="answer" ng-model="answer" ui-tinymce="tinymceOptions"></textarea>
									</div>
								</div>
								<div class="col-12 col-md-12 col-lg-12 text-right">
									<div class="form-group">
										<button class="btn btn-warning btn-lg" ng-click="redirectList()" type="button">Cancel</button>
										<button class="btn btn-danger btn-lg" type="submit">Save</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>