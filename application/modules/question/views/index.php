<style>
	.loader-img {
		width: 25px !important;
	}

	.img-custom {
		height: 25%;
		width: 25%;
	}
</style>
<div class="main-content" ng-controller="question" id="question">
	<section class="section">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row mt-2">
							<div class="col-lg-6 col-md-6 col-sm-6 ">
								<h6><i class="far fa-question-circle"></i> Question</h6>
							</div>
						</div>
					</div>
				</div>
				<div class="card card-statistic-2">
					<div class="card-stats p-3">
						<div class="row">
							<div class="col-12 col-md-12 col-lg-12">
								<form role="search" class="main align-items-center">
									<div class="form-group">
										<input type="search" class="form-control" placeholder="search question..." ng-model="keyword" ng-model-options="{debounce: 500}" ng-change="search()">
									</div>
								</form>
								<div id="accordion" ng-show="message == null" dir-paginate="(key, value) in data|itemsPerPage:itemsPerPage" total-items="total_count" current-page="curPage" pagination-id="paginateID">
									<div class="accordion">
										<div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-1" aria-expanded="true">
											<h4 ng-bind="value.question"></h4>
										</div>
										<div class="accordion-body collapse" id="panel-body-1" data-parent="#accordion">
											<div ng-bind-html="value.answer"></div>
										</div>
									</div>
								</div>
								<dir-pagination-controls max-size="8" template-url="<?= base_url('assets/js/angular/') ?>/dirPagination.tpl.html" direction-links="true" pagination-id="paginateID" boundary-links="true" on-page-change="getQuestion(newPageNumber)">
								</dir-pagination-controls>
								<div class="text-center" ng-show="message != null">
									<img src="<?= $image; ?>" class="img-custom">
									<p ng-bind-html="message"></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>