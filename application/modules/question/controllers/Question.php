<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question extends Tessa_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('question_model');
	}

	public function index_get()
	{
		$this->data['title'] = 'Question ~ iTessa';

		$this->data['version'] = $this->uri->segment(1);

		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];

		$this->data['js'] = array(
			'assets/js/app/question.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
		);

		$this->data['image'] = base_url('assets/img/question.png');

		$this->template->load($this->data, null, 'index');
	}

	public function listQuestion_get()
	{
		$this->data['title'] = 'List Question ~ iTessa';

		$this->data['version'] = $this->uri->segment(1);

		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];

		$this->data['js'] = array(
			'assets/js/app/question.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
		);

		$this->data['image'] = base_url('assets/img/question.png');

		$this->template->load($this->data, null, 'list_question');
	}

	public function answer_get()
	{
		$this->data['title'] = 'Answer Question ~ iTessa';

		$this->data['version'] = $this->uri->segment(1);

		$this->data['id'] = $this->get('id');

		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];

		$this->data['js'] = array(
			'assets/js/app/question.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
		);

		$this->template->load($this->data, null, 'answer_question');
	}


	public function getQuestion_get()
	{
		$return = $this->question_model->getQuestion($this->get());
		$this->response($return, $return['status']);
	}


	public function prosesGetQuestionByID_get()
	{
		$return = $this->question_model->prosesGetQuestionByID($this->get());
		$this->response($return, $return['status']);
	}

	public function requestQuestion_get()
	{
		$this->data['title'] = 'Request Question ~ iTessa';


		$this->data['version'] = $this->uri->segment(1);

		$this->data['css'] = [
			'assets/plugins/animate/animate.min.css',
		];

		$this->data['js'] = array(
			'assets/js/app/question.js?' . rand(),
			'assets/plugins/sweetalert2/dist/sweetalert2.all.min.js',
		);

		$this->data['image'] = base_url('assets/img/question.png');

		$this->template->load($this->data, null, 'request_question');
	}

	public function requestQuestion_post()
	{
		$return = $this->question_model->requestQuestion($this->post());
		$this->response($return, $return['status']);
	}

	public function saveAnswer_post()
	{
		$return = $this->question_model->saveAnswer($this->post());
		$this->response($return, $return['status']);
	}

	public function changeStatus_post()
	{
		$return = $this->question_model->changeStatus($this->post());
		$this->response($return);
	}
}
