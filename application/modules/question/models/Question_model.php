<?php
class Question_model extends CI_Model
{
    public function getQuestion($params)
    {
        $page = json_decode($params['page']);
        $start = ($params['offset'] - 1) * $params['limit'];

        $this->db->select('eq.id,eq.question,eq.answer,eq.status,eq.public_status,eq.rating_answer,eq.question_time,eq.answer_time,epd.full_name name_question,emp_answerer.full_name name_answerer')
            ->from('employee_question eq')
            ->join('employee_personal_data epd', 'epd.id = eq.id_user_question', 'left')
            ->join('employee_personal_data emp_answerer', 'emp_answerer.id = eq.id_user_answerer', 'left');
        if ($page[2] == 'question' && count($page) == 3) {
            $this->db->where('eq.status', 'answered')
                ->where('eq.public_status', 1);
        }

        if (!empty($params['keyword_qnh']) && empty($page[3])) {
            $this->db->group_start()
                ->like('eq.question', $params['keyword_qnh'])
                ->or_like('epd.full_name', $params['keyword_qnh']);
            $this->db->group_end();
        }

        if (!empty($params['keyword_qnh']) && !empty($page[3])) {
            $this->db->group_start()
                ->like('eq.question', $params['keyword_qnh'])
                ->or_like('epd.full_name', $params['keyword_qnh'])
                ->or_like('eq.status', $params['keyword_qnh']);
            $this->db->group_end();
        }


        $get_data = $this->db->limit($params['limit'], $start)->order_by('eq.answer_time', 'desc')->get()->result_array();

        $this->db->select('eq.id,eq.question,eq.answer,eq.status,eq.public_status,eq.rating_answer,eq.question_time,eq.answer_time,epd.full_name name_question,emp_answerer.full_name name_answerer')
            ->from('employee_question eq')
            ->join('employee_personal_data epd', 'epd.id = eq.id_user_question', 'left')
            ->join('employee_personal_data emp_answerer', 'emp_answerer.id = eq.id_user_answerer', 'left');
        if ($page[2] == 'question' && count($page) == 3) {
            $this->db->where('eq.status', 'answered')
                ->where('eq.public_status', 1);
        }

        if (!empty($params['keyword_qnh']) && empty($page[3])) {
            $this->db->group_start()
                ->like('eq.question', $params['keyword_qnh'])
                ->or_like('epd.full_name', $params['keyword_qnh']);
            $this->db->group_end();
        }

        if (!empty($params['keyword_qnh']) && !empty($page[3])) {
            $this->db->group_start()
                ->like('eq.question', $params['keyword_qnh'])
                ->or_like('epd.full_name', $params['keyword_qnh'])
                ->or_like('eq.status', $params['keyword_qnh']);
            $this->db->group_end();
        }


        $count_data = $this->db->order_by('eq.answer_time', 'desc')->get()->num_rows();

        if ($count_data < 1) {
            return [
                'status' => 400,
                'message' => !empty($page[3]) ? 'Question Empty!' : "Question not found, want to try asking? ",
                'count' => $count_data,
                'data' => [],
            ];
        }

        foreach ($get_data as $key => $value) {
            $data[] = [
                'answer' => $value['answer'],
                'answer_time' => date('Y-m-d H:i', strtotime($value['answer_time'])),
                'id' => $value['id'],
                'name_answerer' => $value['name_answerer'],
                'name_question' => $value['name_question'],
                'public_status' => $value['public_status'] != 1 ? false : true,
                'question' => $value['question'],
                'question_time' => date('Y-m-d H:i', strtotime($value['question_time'])),
                'rating_answer' => $value['rating_answer'],
                'status' => ucwords($value['status']) == 'Answered' ? '<span class="badge badge-pill badge-success">' . ucwords($value['status']) . '</span>' : '<span class="badge badge-pill badge-danger">' . ucwords($value['status']) . '</span>',
            ];
        }

        return [
            'status' => 200,
            'message' => null,
            'data' => $data,
            'count' => $count_data
        ];
    }

    public function prosesGetQuestionByID($params)
    {
        $this->db->select('eq.question,eq.answer,eq.status,eq.public_status,eq.rating_answer,eq.question_time,eq.answer_time,epd.full_name name_question,emp_answerer.full_name name_answerer')
            ->from('employee_question eq')
            ->join('employee_personal_data epd', 'epd.id = eq.id_user_question', 'left')
            ->join('employee_personal_data emp_answerer', 'emp_answerer.id = eq.id_user_answerer', 'left')
            ->where('eq.id', $params['id']);
        $get_data = $this->db->get()->row();

        return [
            'status' => !empty($get_data) ? 200 : 500,
            'message' => !empty($get_data) ? 'data available' : 'data not found, please back to list!',
            'data' => !empty($get_data) ? $get_data : [],
        ];
    }

    public function requestQuestion($params)
    {
        $this->db->select('id')
            ->like($params)
            ->where('status', 'answered')
            ->where('public_status', 1)
            ->where('answer_time is not null');
        $check_question = $this->db->get('employee_question')->num_rows();
        if ($check_question > 0) {
            return [
                'status' => 400,
                'message' => 'Question already exists!'
            ];
        }

        $params['id_user_question'] = users_sessions('employee_id');
        $params['question_time'] = date('Y-m-d H:i:s');
        $params['status'] = 'unanswered';
        $params['public_status'] = false;

        if ($this->db->insert('employee_question', $params)) {
            return [
                'message' => 'Request question success',
                'status' => 200
            ];
        }

        return [
            'message' => 'Internal Server Error, error code: 500',
            'status' => 500,
        ];
    }

    public function saveAnswer($params)
    {
        $id = $params['id'];
        unset($params['id']);
        $params['status'] = 'answered';
        $params['id_user_answerer'] = users_sessions('employee_id');
        $params['answer_time'] = date('Y-m-d H:i:s');
        if ($this->db->where('id', $id)->update('employee_question', $params)) {
            return [
                'status' => 200,
                'message' => 'Success add answer',
            ];
        }
        return [
            'status' => 500,
            'message' => 'failed add answer with error code: 500!'
        ];
    }

    public function changeStatus($params)
    {
        $id = $params['id'];
        unset($params['id']);
        if ($this->db->where('id', $id)->update('employee_question', $params)) {
            return [
                'status' => 200,
                'message' => 'change status public success',
            ];
        }

        return [
            'status' => 500,
            'message' => 'change status public failed, error code: 500',
        ];
    }
}
