
<div class="main-content" ng-controller="biodata">
    <section class="section">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats p-3">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <ul class="nav nav-pills mb-2" id="pills-tab" role="tablist">
                                    <li class="nav-item col-lg-4 text-center" role="presentation">
                                        <a class="nav-link active" id="pills-personal-tab" data-toggle="pill" href="#pills-personal" role="tab" aria-controls="pills-personal" aria-selected="true"><i class="fas fa-user"></i> Personal Data</a>
                                    </li>
                                    <li class="nav-item col-lg-4 text-center" role="presentation">
                                        <a class="nav-link " id="pills-educational-tab" data-toggle="pill" href="#pills-educational" role="tab" aria-controls="pills-educational" aria-selected="false"><i class="fas fa-graduation-cap"></i> Educational Background</a>
                                    </li>
                                    <li class="nav-item col-lg-4 text-center" role="presentation">
                                        <a class="nav-link " id="pills-family-tab" data-toggle="pill" href="#pills-family" role="tab" aria-controls="pills-family" aria-selected="false"><i class="fas fa-id-card-alt"></i> Family</a>
                                    </li>
                                    <!-- <li class="nav-item col-lg-3 text-center" role="presentation">
                                        <a class="nav-link" id="pills-document-tab" data-toggle="pill" href="#pills-document" role="tab" aria-controls="pills-document" aria-selected="false"><i class="fas fa-file-alt"></i> Document</a>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-personal" role="tabpanel" aria-labelledby="pills-personal-tab">
                                <form>
                                
                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name">Name <span class="text-danger">(*)</span></label>
                                                        <input type="email" class="form-control" id="name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="place_birth">Place of Birth <span class="text-danger">(*)</span></label>
                                                        <input type="text" class="form-control" id="place_birth">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="date_birth">Date of Birth <span class="text-danger">(*)</span></label>
                                                        <input type="date" class="form-control" id="date_birth">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="status_marriage">Status Marriage <span class="text-danger">(*)</span></label>
                                                        <select class="form-control" name="status_marriage" id="status_marriage">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="single">Single</option>
                                                            <option value="marriage">Marriage</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <div class="form-group">
                                                                <label for="id_card_type">ID Card Type<span class="text-danger">(*)</span></label>
                                                                <select class="form-control" class="form-control" name="id_card_type" id="id_card_type">
                                                                    <option value="" selected> - Choose -</option>
                                                                    <option value="ektp">e-KTP</option>
                                                                    <option value="nonelectorinc">Non Electronic</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-9 col-md-9 col-sm-9">
                                                            <div class="form-group">
                                                                <label for="identity_number">Identity Number<span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="identity_number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="npwp">NPWP <span class="text-secondary">(optional)</span></label>
                                                        <input type="text" class="form-control" id="npwp">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="id_card_issue_date">ID Card Issue Date<span class="text-danger">(*)</span></label>
                                                                <input type="date" class="form-control" id="id_card_issue_date">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="id_card_issue_place">ID Card Issue Place<span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="id_card_issue_place">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="npwp_issue_date">NPWP Issue Date <span class="text-secondary">(optional)</span></label>
                                                        <input type="text" class="form-control" id="npwp_issue_date">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="employment_card">Employment Card <span class="text-secondary">(optional)</span></label>
                                                        <input type="text" class="form-control" id="employment_card">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="eye_glasses">Eye Glasses <span class="text-danger">(*)</span></label>
                                                                <select class="form-control" class="form-control" name="eye_glasses" id="eye_glasses">
                                                                    <option value="" selected> - Choose -</option>
                                                                    <option value="yes">Yes</option>
                                                                    <option value="no">No</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="blood_type">Blood Type <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="blood_type">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="religion">Religion <span class="text-danger">(*)</span></label>
                                                        <select class="form-control" class="form-control" name="religion" id="religion">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="islam">Islam</option>
                                                            <option value="kristen">Kristen</option>
                                                            <option value="budha">Budha</option>
                                                            <option value="hindu">Hindu</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="phone_number">Phone Number <span class="text-danger">(*)</span></label>
                                                        <input type="number" class="form-control" id="phone_number">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="bank_account">Bank Account <span class="text-danger">(*)</span></label>
                                                        <input type="text" class="form-control" id="bank_account">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="bank_account_number">Bank Account Number<span class="text-danger">(*)</span></label>
                                                        <input type="number" class="form-control" id="bank_account_number">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-primary">Emergency Contact Detail</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="name_emergency">Name<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="name_emergency">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="relationship_emergency">Relationship<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="relationship_emergency">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="phone_number_emergency">Phone Number<span class="text-danger"> (*)</span></label>
                                                        <input type="number" class="form-control" id="phone_number_emergency">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="email_emergency">Email<span class="text-danger"> (*)</span></label>
                                                        <input type="email" class="form-control" id="email_emergency">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-primary">Main Address <span class="text-secondary"> (based on ID Card)</span></h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="main_address">Address<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="main_address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="main_subdistrict">Subdistrict<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="main_subdistrict">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="main_district">District<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="main_district">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="main_province">Province<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="main_province">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="main_city">City<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="main_city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="main_postal_code">Postal Code<span class="text-danger"> (*)</span></label>
                                                        <input type="number" class="form-control" id="main_postal_code">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-primary">Secondary Address</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="secondary_address">Address<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="secondary_address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="secondary_subdistrict">Subdistrict<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="secondary_subdistrict">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="secondary_district">District<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="secondary_district">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="secondary_province">Province<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="secondary_province">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="secondary_city">City<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="secondary_city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="secondary_postal_code">Postal Code<span class="text-danger"> (*)</span></label>
                                                        <input type="number" class="form-control" id="secondary_postal_code">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-primary">Emergency Address</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="emergency_address">Address<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="emergency_address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="emergency_subdistrict">Subdistrict<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="emergency_subdistrict">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="emergency_district">District<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="emergency_district">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="emergency_province">Province<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="emergency_province">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="emergency_city">City<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="emergency_city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="emergency_postal_code">Postal Code<span class="text-danger"> (*)</span></label>
                                                        <input type="number" class="form-control" id="emergency_postal_code">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <button type="button" class="btn btn-primary btn-lg btn-save-personal">Submit</button>
                                                    <button type="button" class="btn btn-secondary btn-lg btn-cancel-personal">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="pills-educational" role="tabpanel" aria-labelledby="pills-educational-tab">
                                <form>
                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Latest Educational Level</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="latest_level">Educational<span class="text-danger"> (*)</span></label>
                                                        <select class="form-control" name="latest_level" id="latest_level">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="elementary">Elementary School</option>
                                                            <option value="junior">Junior High School</option>
                                                            <option value="senior">Senior High School</option>
                                                            <option value="diploma">Associate Degree</option>
                                                            <option value="bachelor">Bachelor Degree</option>
                                                            <option value="master">Master Degree</option>
                                                            <option value="doctoral">Doctoral Degree</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="latest_educational">Institution<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="latest_educational">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="latest_faculty">Faculty Study Program<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="latest_faculty">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="latest_start">Start Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="latest_start">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="latest_finish">Finish Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="latest_finish">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Educational Level</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_level">Educational<span class="text-danger"> (*)</span></label>
                                                        <select class="form-control" name="educational_level" id="educational_level">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="elementary">Elementary School</option>
                                                            <option value="junior">Junior High School</option>
                                                            <option value="senior">Senior High School</option>
                                                            <option value="diploma">Associate Degree</option>
                                                            <option value="bachelor">Bachelor Degree</option>
                                                            <option value="master">Master Degree</option>
                                                            <option value="doctoral">Doctoral Degree</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_institution">Institution<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_institution">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_faculty">Faculty Study Program<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_faculty">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_start">Start Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_start">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_finish">Finish Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_finish">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Educational Level</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_two_level">Educational<span class="text-danger"> (*)</span></label>
                                                        <select class="form-control" name="educational_two_level" id="educational_two_level">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="elementary">Elementary School</option>
                                                            <option value="junior">Junior High School</option>
                                                            <option value="senior">Senior High School</option>
                                                            <option value="diploma">Associate Degree</option>
                                                            <option value="bachelor">Bachelor Degree</option>
                                                            <option value="master">Master Degree</option>
                                                            <option value="doctoral">Doctoral Degree</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_two_institution">Institution<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_two_institution">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_two_faculty">Faculty Study Program<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_two_faculty">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_two_start">Start Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_two_start">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_two_finish">Finish Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_two_finish">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Educational Level</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_three_level">Educational<span class="text-danger"> (*)</span></label>
                                                        <select class="form-control" name="educational_three_level" id="educational_three_level">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="elementary">Elementary School</option>
                                                            <option value="junior">Junior High School</option>
                                                            <option value="senior">Senior High School</option>
                                                            <option value="diploma">Associate Degree</option>
                                                            <option value="bachelor">Bachelor Degree</option>
                                                            <option value="master">Master Degree</option>
                                                            <option value="doctoral">Doctoral Degree</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_three_institution">Institution<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_three_institution">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_three_faculty">Faculty Study Program<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_three_faculty">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_three_start">Start Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_three_start">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_three_finish">Finish Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_three_finish">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Educational Level</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_four_level">Educational<span class="text-danger"> (*)</span></label>
                                                        <select class="form-control" name="educational_four_level" id="educational_four_level">
                                                            <option value="" selected> - Choose -</option>
                                                            <option value="elementary">Elementary School</option>
                                                            <option value="junior">Junior High School</option>
                                                            <option value="senior">Senior High School</option>
                                                            <option value="diploma">Associate Degree</option>
                                                            <option value="bachelor">Bachelor Degree</option>
                                                            <option value="master">Master Degree</option>
                                                            <option value="doctoral">Doctoral Degree</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_four_institution">Institution<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_four_institution">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="educational_four_faculty">Faculty Study Program<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="educational_four_faculty">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_four_start">Start Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_four_start">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="educational_four_finish">Finish Date<span class="text-danger"> (*)</span></label>
                                                                <input type="date" class="form-control" id="educational_four_finish">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <button type="button" class="btn btn-primary btn-lg btn-save-educational">Submit</button>
                                                    <button type="button" class="btn btn-secondary btn-lg btn-cancel-educational">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="pills-family" role="tabpanel" aria-labelledby="pills-family-tab">
                                <form>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="card card-statistic-2">
                                                <div class="card-stats p-3">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h6 class="text-left text-success">Mother</h6>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                                            <div class="form-group">
                                                                <label for="mother_name">Full Name <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="mother_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="mother_place_of_birth">Place of Birth <span class="text-danger">(*)</span></label>
                                                                        <input type="text" class="form-control" id="mother_place_of_birth">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="mother_date_of_birth">Date of Birth <span class="text-danger">(*)</span></label>
                                                                        <input type="date" class="form-control" id="mother_date_of_birth">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mother_educational">Latest Educational Level <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="mother_educational">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mother_occupation">Occupation <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="mother_occupation">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="card card-statistic-2">
                                                <div class="card-stats p-3">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h6 class="text-left text-success">Father</h6>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                                            <div class="form-group">
                                                                <label for="father_name">Full Name <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="father_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="father_place_of_birth">Place of Birth <span class="text-danger">(*)</span></label>
                                                                        <input type="text" class="form-control" id="father_place_of_birth">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="father_date_of_birth">Date of Birth <span class="text-danger">(*)</span></label>
                                                                        <input type="date" class="form-control" id="father_date_of_birth">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="father_educational">Latest Educational Level <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="father_educational">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="father_occupation">Occupation <span class="text-danger">(*)</span></label>
                                                                <input type="text" class="form-control" id="father_occupation">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Parents Address</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_address">Address<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="parents_address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_subdistrict">Subdistrict<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="parents_subdistrict">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_district">District<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="parents_district">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_province">Province<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="parents_province">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_city">City<span class="text-danger"> (*)</span></label>
                                                        <input type="text" class="form-control" id="parents_city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_postal_code">Postal Code<span class="text-danger"> (*)</span></label>
                                                        <input type="number" class="form-control" id="parents_postal_code">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- In Law -->
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="card card-statistic-2">
                                                <div class="card-stats p-3">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h6 class="text-left text-success">Mother In Law</h6>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                                            <div class="form-group">
                                                                <label for="mother_law_name">Full Name</label>
                                                                <input type="text" class="form-control" id="mother_law_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="mother_law_place_of_birth">Place of Birth</label>
                                                                        <input type="text" class="form-control" id="mother_law_place_of_birth">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="mother_law_date_of_birth">Date of Birth</label>
                                                                        <input type="date" class="form-control" id="mother_law_date_of_birth">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mother_law_educational">Latest Educational Level</label>
                                                                <input type="text" class="form-control" id="mother_law_educational">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="mother_law_occupation">Occupation</label>
                                                                <input type="text" class="form-control" id="mother_law_occupation">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="card card-statistic-2">
                                                <div class="card-stats p-3">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h6 class="text-left text-success">Father In Law</h6>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                                            <div class="form-group">
                                                                <label for="father_law_name">Full Name</label>
                                                                <input type="text" class="form-control" id="father_law_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-8">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="father_law_place_of_birth">Place of Birth</label>
                                                                        <input type="text" class="form-control" id="father_law_place_of_birth">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="father_law_date_of_birth">Date of Birth</label>
                                                                        <input type="date" class="form-control" id="father_law_date_of_birth">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="father_law_educational">Latest Educational Level</label>
                                                                <input type="text" class="form-control" id="father_law_educational">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="father_law_occupation">Occupation</label>
                                                                <input type="text" class="form-control" id="father_law_occupation">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <h6 class="text-left text-success">Parents In Law Address</h6>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_law_address">Address</label>
                                                        <input type="text" class="form-control" id="parents_law_address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_law_subdistrict">Subdistrict</label>
                                                        <input type="text" class="form-control" id="parents_law_subdistrict">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_law_district">District</label>
                                                        <input type="text" class="form-control" id="parents_law_district">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_law_province">Province</label>
                                                        <input type="text" class="form-control" id="parents_law_province">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_law_city">City</label>
                                                        <input type="text" class="form-control" id="parents_law_city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label for="parents_law_postal_code">Postal Code</label>
                                                        <input type="number" class="form-control" id="parents_law_postal_code">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- SPOUSE and Children -->
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="card card-statistic-2">
                                                <div class="card-stats p-3">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h6 class="text-left text-success">Spouse</h6>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="spouse_name">Full Name</label>
                                                                <input type="text" class="form-control" id="spouse_name">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="spouse_place_of_birth">Place of Birth</label>
                                                                <input type="text" class="form-control" id="spouse_place_of_birth">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="spouse_date_of_birth">Date of Birth</label>
                                                                <input type="date" class="form-control" id="spouse_date_of_birth">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="spouse_educational">Latest Educational Level</label>
                                                                <input type="text" class="form-control" id="spouse_educational">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="spouse_occupation">Occupation</label>
                                                                <input type="text" class="form-control" id="spouse_occupation">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <div class="form-group">
                                                                <label for="spouse_married">Married Date</label>
                                                                <input type="date" class="form-control" id="spouse_married">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="card card-statistic-2">
                                                <div class="card-stats p-3">
                                                    <div class="row mb-3">
                                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                                            <h6 class="text-left text-success">Children</h6>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 text-right">
                                                            <button type="button" class="btn btn-success btn-sm btn-add-children"><i class="fas fa-plus"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                                            <table class="table table-sm table-hover table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th rowspan="2" style="vertical-align: middle !important">Full Name</th>
                                                                        <th colspan="2" style="vertical-align: middle !important" class="text-center">Birth</th>
                                                                        <th rowspan="2" style="vertical-align: middle !important">Latest Educational</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="text-center">Place</th>
                                                                        <th class="text-center">Date</th>
                                                                    </tr>
                                                                </thead>
                                                                <body>
                                                                    <tr>
                                                                        <td>
                                                                            <input type="text" class="form-control" id="children_name">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" class="form-control" id="children_place_birth">
                                                                        </td>
                                                                        <td>
                                                                            <input type="date" class="form-control" id="children_date_birth">
                                                                        </td>
                                                                        <td>
                                                                            <select class="form-control" name="children_educational" id="children_educational">
                                                                                <option value="elementary">Elementary School</option>
                                                                                <option value="junior">Junior High School</option>
                                                                                <option value="senior">Senior High School</option>
                                                                                <option value="diploma">Associate Degree</option>
                                                                                <option value="bachelor">Bachelor Degree</option>
                                                                                <option value="master">Master Degree</option>
                                                                                <option value="doctoral">Doctoral Degree</option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                </body>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <div class="card card-statistic-2">
                                        <div class="card-stats p-3">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <button type="button" class="btn btn-primary btn-md btn-save-family">Submit</button>
                                                    <button type="button" class="btn btn-secondary btn-md btn-cancel-family">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- <div class="tab-pane fade" id="pills-document" role="tabpanel" aria-labelledby="pills-document-tab">
                                Document
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>