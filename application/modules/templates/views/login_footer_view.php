
    <script src="<?= base_url() ?>assets/iform/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/iform/js/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/iform/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/iform/js/main.js"></script>
    <script type="text/javascript">
        <?php if($this->session->flashdata('success')):?>
            swal("Success", '<?php echo $this->session->flashdata('success'); ?>', "success");
        <?php endif;?>
        <?php if($this->session->flashdata('error')):?>
            swal("Oops!", '<?php echo $this->session->flashdata('error'); ?>', "error");
        <?php endif;?>
    </script>
</html>