<body ng-app="itessa">
	<div id="app">
		<div class="main-wrapper">
			<div class="navbar-bg"></div>
			<nav class="navbar navbar-expand-lg main-navbar">
				<form class="form-inline mr-auto">
					<ul class="navbar-nav mr-3">
						<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
					</ul>
					<div class="search-element">
						<h5 class="text-white mt-2" style="height: 10px;">ITESSA</h5>
						<small>Integrated Telin Employee Self Service Administration</small>
					</div>
				</form>
				<ul class="navbar-nav navbar-right">
					<li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
							<img alt="image" src="<?= $users['photo_profile'] ?>" class="rounded-circle mr-1">
							<div class="d-sm-none d-lg-inline-block">Hi, <?= $users["nick_name"] ?></div>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<div class="dropdown-title text-uppercase">
								<p>
									<small>Login sebagai:</small> <br>
									<b><?= $users["role_name"] ?></b>
								</p>
							</div>
							<a href="features-profile.html" class="dropdown-item has-icon">
								<i class="far fa-user"></i> Profile
							</a>
							<a href="features-activities.html" class="dropdown-item has-icon">
								<i class="fas fa-bolt"></i> Activities
							</a>
							<a href="features-settings.html" class="dropdown-item has-icon">
								<i class="fas fa-cog"></i> Settings
							</a>
							<div class="dropdown-divider"></div>
							<a href="<?= base_url("index.php/logout"); ?>" class="dropdown-item has-icon text-danger">
								<i class="fas fa-sign-out-alt"></i> Logout
							</a>
						</div>
					</li>
				</ul>
			</nav>
			<div class="main-sidebar">
				<aside id="sidebar-wrapper">
					<div class="sidebar-brand mt-2">
						<a href="index.html"><img style="width: 30%;" src="<?= base_url() ?>assets/img/logo.png" class="mr-0"> </a>
					</div>
					<div class="sidebar-brand sidebar-brand-sm">
						<a href="index.html">IT</a>
					</div>
					<ul class="sidebar-menu mt-5">

						<?php if ($users["role_id"] == 2) { ?>

							<li class="<?= $version == 'biodata' ? 'active' : '' ?>">
								<a class="nav-link" href="<?= base_url("biodata") ?>">
									<i class="fas fa-clipboard-list"></i> <span>Biodata</span>
								</a>
							</li>

							<li class="nav-item dropdown <?= $version == 'employee' ? 'active' : '' ?>">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-user-tie"></i><span>Employee</span></a>
								<ul class="dropdown-menu">
									<li class="nav-item">
										<a class="nav-link" href="<?= base_url('employee/detail'); ?>">
											<span>Detail</span>
										</a>
									</li>

								</ul>
							</li>

						<?php } ?>

						<?php if ($users['role_id'] == 3) : ?>
							<li class="<?= $version == 'employee' ? 'active' : '' ?>">
								<a class="nav-link" href="<?= base_url("employee/detail") ?>">
									<i class="fas fa-user-tie"></i><span>Employee</span>
								</a>
							</li>
						<?php endif; ?>

						<?php if ($users["role_id"] == 1) { ?>
							<li class="<?= $version == 'candidate' ? 'active' : '' ?>" class="nav-item dropdown">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-chalkboard-teacher"></i><span>Recruitment</span></a>
								<ul class="dropdown-menu">
									<li><a class="nav-link" href="<?= base_url("candidate") ?>">Candidate</a></li>
								</ul>
							</li>

						<?php } ?>


						<?php if ($users["role_id"] == 1) { ?>
							<li class="nav-item dropdown <?= $version == 'human' ? 'active' : '' ?>">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Human Capital</span></a>
								<ul class="dropdown-menu">
									<?php if ($users["role_id"] == 2) { ?>
										<li><a class="nav-link" href="<?= base_url() ?>">Request to HC</a></li>
									<?php } ?>
									<?php if ($users["role_id"] == 1) { ?>
										<li><a class="nav-link" href="<?= base_url() ?>">Approval</a></li>
									<?php } ?>
								</ul>
							</li>
						<?php } ?>
						<?php if ($users['role_id'] == 1 || $users['role_id'] == 2 || $users['role_id'] == 3 || $users['role_id'] == 4) { ?>
							<?php
							$faq_menu = $users['role_id'] == 1 ? base_url('faq') : base_url();
							$question_menu = $users['role_id'] == 1 ? base_url('question/listQuestion') : base_url('question'); ?>
							<li class="nav-item dropdown <?= $version == 'question' || $version == 'faq' ? 'active' : '' ?>">
								<a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>FAQ</span></a>
								<ul class="dropdown-menu">
									<li class="nav-item  <?= $version == 'faq' ? 'active' : '' ?>">
										<a class="nav-link" href="<?= $faq_menu ?>">
											<span>FAQ</span>
										</a>
									</li>
									<li class="nav-item  <?= $version == 'question' ? 'active' : '' ?>">
										<a class="nav-link" href="<?= $question_menu; ?>">
											<span>Question</span>
										</a>
									</li>

								</ul>
							</li>
						<?php } ?>

						<?php if ($users["role_id"] == 1) { ?>
							<li class="<?= $version == 'user' ? 'active' : '' ?>">
								<a class="nav-link" href="<?= base_url() ?>">
									<i class="fas fa-users"></i> <span>User Managements</span>
								</a>
							</li>
						<?php } ?>
					</ul>
					<!-- <?php if ($users["role_id"] == 1) { ?>
                        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                            <a href="#" class="btn btn-danger btn-lg btn-block btn-icon-split">
                                <i class="fas fa-rocket"></i> <small>Documentation Knowledge</small>
                            </a>
                        </div>
                    <?php } ?> -->
				</aside>
			</div>

			<?php if (!empty($this->session->flashdata())) { ?>
				<div class="main-flashdata" style="min-height: 0 !important;">
					<?php if (!empty($this->session->flashdata('alert-success'))) : ?>
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<div class="alert-icon contrast-alert"><i class="fa fa-check"></i></div>
							<div class="alert-message">
								<span><strong>Berhasil!</strong> - <?= $this->session->flashdata('alert-success') ?></span>
							</div>
						</div>
					<?php endif; ?>
					<?php if (!empty($this->session->flashdata('alert-warning'))) : ?>
						<div class="alert alert-warning alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<div class="alert-icon contrast-alert"><i class="fa fa-exclamation-triangle"></i></div>
							<div class="alert-message">
								<span><strong>Perhatian!</strong> - <?= $this->session->flashdata('alert-warning') ?></span>
							</div>
						</div>
					<?php endif; ?>

					<?php if (!empty($this->session->flashdata('alert-danger'))) : ?>
						<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<div class="alert-icon contrast-alert"><i class="fa fa-times"></i></div>
							<div class="alert-message">
								<span><strong>Gagal!</strong> - <?= $this->session->flashdata('alert-danger') ?></span>
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php } ?>
			<?= $body ?>

		</div>
	</div>
</body>