<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

class Login extends Tessa_Controller
{


	function __construct()
	{
		parent::__construct();
	}


	public function index_get()
	{

		// Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Login';
		$this->data['page'] = 'login';

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
		);

		$this->template->load($this->data, "login", 'index');
	}

	public function index_post()
	{

		$this->load->model('UsersDB');

		$error = true;

		if ($_POST['username'] == null and $_POST['password'] == null) {

			$this->response(array('error' => true, 'message' => 'Please fill username and password!'));
		} else {

			$users = $this->UsersDB->getUser($_POST['username']);
			if (!empty($users)) { // tidak kosong

				$check = setDecrypt($users->psswd, $_POST['password']);

				if ($check) {

					$data = array(
						'last_login' => date("Y-m-d H:i:s")
					);

					$this->db->where('username', $_POST['username']);
					$this->db->update('users', $data);


					$this->session->set_userdata([
						'authTessa' => [
							'id'		=> $users->id,
							'username'	=> $users->username,
							'role_id'		=> $users->role_id,
							'role_name'		=> $users->role_name,
							'id_employee' => $users->id_employee,
							'full_name' => $users->full_name,
							'nick_name' => $users->nick_name,
							'date_of_birth' => $users->date_of_birth,
							'sex' => $users->sex,
							'marital_status' => $users->marital_status,
							'nationality' => $users->nationality,
							'citizen_number' => $users->citizen_number,
							'blood_type' => $users->blood_type,
							'religion' => $users->religion,
							'mobile_number' => $users->mobile_number,
							'email' => $users->email,
							'photo_profile' => !empty($users->photo_profile) ? $users->photo_profile : base_url() . 'assets/img/avatar/avatar-1.png',
							'citizen_file' => $users->citizen_file,
						]
					]);

					$this->session->set_flashdata('success', "User valid, please wait!");

					if ($users->role_id == 1) {
						redirect(base_url('candidate'));
					} elseif ($users->role_id == 2) {
						redirect(base_url("biodata"));
					} elseif ($users->role_id == 3) {
						redirect(base_url("employee/detail"));
					}

					$error = "success";
					$message = 'User valid, please wait!';
				} else {
					$error = "error";
					$message = 'Username or password wrong!';

					$this->session->set_flashdata('error', "Username or password wrong!");
					// redirect(base_url());
				}
			} else {
				$error = "error";
				$message = 'User not found!';

				$this->session->set_flashdata('error', "User not found!");
				// redirect(base_url());
			}


			$this->session->set_flashdata($error, $message);
			redirect(base_url("login"));
		}
	}

	public function logout_get()
	{
		$this->session->sess_destroy();
		redirect(base_url("login"));
	}
}
