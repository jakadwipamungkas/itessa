<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class UsersDB extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getUser($username)
    {
        $select = [
            'users.id',
            'users.username',
            'users.psswd',
            'users.role_id',
            'users_role.role_name',
            'users_role.role_access',
            'employee_personal_data.id id_employee',
            'employee_personal_data.full_name',
            'employee_personal_data.nick_name',
            'employee_personal_data.date_of_birth',
            'employee_personal_data.sex',
            'employee_personal_data.marital_status',
            'employee_personal_data.nationality',
            'employee_personal_data.citizen_number',
            'employee_personal_data.blood_type',
            'employee_personal_data.religion',
            'employee_personal_data.mobile_number',
            'employee_personal_data.email',
            'tbl_profile.url_document photo_profile',
            'tbl_file_citizen.url_document citizen_file'
        ];
        // $query = $this->db->select("users.*, users_role.*,employee_personal_data.*,tbl_profile.url_document photo_profile,tbl_file_citizen.url_document citizen_file")
        $query = $this->db->select($select)
            ->from("users")
            ->join("users_role", "users_role.id = users.role_id", "inner")
            ->join("employee_personal_data", "employee_personal_data.id_users = users.id", 'left')
            ->join("ref_file_uploaded as tbl_profile", "tbl_profile.id = employee_personal_data.id_photo_profile", "left")
            ->join("ref_file_uploaded as tbl_file_citizen", "tbl_file_citizen.id = employee_personal_data.id_photo_citizen_number", "left")
            ->where("users.username", $username)
            ->get()
            ->first_row();
        return $query;
    }
}
