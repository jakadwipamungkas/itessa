
    <div class="form-body">
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img src="<?= base_url() ?>assets/img/logo.png" alt="" class="img-fluid mb-5" style="width: 30%; float: left;">
                    <img src="<?= base_url() ?>assets/iform/images/graphic1.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="website-logo-inside">
                            <!-- <a href="index-2.html"> -->
                                <div class="logo-telin">
                                    <!-- <h2 class="text-white">iTessa</h2> -->
                                    <h3>iTessa</h3>
                                    <small class="text-white"><i>Integrated Telin Employee Self Service Administration</i></small>
                                </div>
                            <!-- </a> -->
                        </div>
                        <div class="page-links">
                            <a href="login9.html" class="active">Login</a>
                            <!-- <a href="register9.html">Register</a> -->
                        </div>
                        <form action="<?= base_url('login') ?>" method="POST">
                            <input class="form-control" type="text" name="username" placeholder="Username" required>
                            <input class="form-control" type="password" name="password" placeholder="Password" required>
                            <div class="form-button">
                                <button id="submit" type="submit" class="ibtn">Login</button> 
                                <!-- <a href="forget9.html">Forget password?</a> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
